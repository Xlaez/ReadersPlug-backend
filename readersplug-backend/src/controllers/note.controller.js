/* eslint-disable no-unused-vars */
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { noteService } = require('../services');
const pick = require('../utils/pick');
const { MESSAGES } = require('../constants/responseMessages');

const saveNote = catchAsync(async (req, res) => {
  // const note = await noteService.createNote();
  const data = { ...req.body, user: req.user._id };
  const note = await noteService.createNote(data);
  if (!note) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(httpStatus.CREATED).json({ mesage: MESSAGES.SUCCESS });
});

const findOneByNotesId = catchAsync(async (req, res) => {
  const note = await noteService.findOneByNotesId(req.params.noteId);
  if (!note) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(httpStatus.OK).json(note);
});

const findAllByUserId = catchAsync(async (req, res) => {
  const filter = { user: req.user._id };
  const options = pick(req.query, ['limit', 'page']);
  const note = await noteService.findAllByUserId(filter, options);
  if (!note) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(httpStatus.OK).json(note);
});

const deleteOneByNotesId = catchAsync(async (req, res) => {
  const note = await noteService.deleteOneByNotesId(req.params.noteId);
  if (!note) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(note);
});
module.exports = {
  saveNote,
  findOneByNotesId,
  findAllByUserId,
  deleteOneByNotesId,
};
