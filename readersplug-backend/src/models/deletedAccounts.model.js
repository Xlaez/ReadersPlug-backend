const { Schema, model } = require('mongoose');
const paginate = require('mongoose-paginate-v2');
const modelNames = require('../constants/modelNames.constants');
const toJSON = require('./plugins/toJSON.plugin');

const schema = new Schema(
  {
    reason: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

schema.plugin(paginate);
schema.plugin(toJSON);

const DeletedAcc = model(modelNames.deletedAcc, schema);
module.exports = DeletedAcc;
