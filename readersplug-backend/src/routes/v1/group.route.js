/* eslint-disable no-unused-vars */
const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const validateAcc = require('../../middlewares/validateUser');
const { groupController } = require('../../controllers');
const { singleUpload, multipleUpload } = require('../../libs/multer');
const { groupValidation } = require('../../validations');
const { isAdmin } = require('../../middlewares/roles');

const router = express.Router();

router.get('/recent-msgs', validateAcc, groupController.getGoupsRecentMsgs);
router.post('/new', validateAcc, singleUpload, validate(groupValidation.createGroup), groupController.createGroup);
router.get('/by-name/:name', validateAcc, groupController.getGroupByName);
router.get('/by-id/:id', validateAcc, groupController.getGroupById);
router.patch('/:groupId/update-logo', singleUpload, validateAcc, groupController.uploadLogo);
router.delete('/:groupId', validateAcc, groupController.deleteGroup);
router.post('/msg', validateAcc, multipleUpload, validate(groupValidation.sendMessage), groupController.sendMessage);
router.patch('/:groupId/msg/mark-seen', validateAcc, groupController.markMessagesSeen);
router.delete('/msg/:msgId', validateAcc, groupController.deleteMessage);
router.put('/:groupId/add-members', validateAcc, validate(groupValidation.addOrRemoveMembers), groupController.addMembers);
router.purge(
  '/:groupId/remove-members',
  validateAcc,
  validate(groupValidation.addOrRemoveMembers),
  groupController.removeMembers
);
router.put('/:groupId/add-admin', validateAcc, validate(groupValidation.addOrRemoveAdmins), groupController.addAdmins);
router.purge(
  '/:groupId/remove-admin',
  validateAcc,
  validate(groupValidation.addOrRemoveAdmins),
  groupController.removeAdmins
);
router.get('/:groupId/msgs', validateAcc, groupController.getMessagesByGroupId);
router.post('/report', validateAcc, validate(groupValidation.reportMessage), groupController.reportMessage);
router.get('/report/:reportedId', validateAcc, isAdmin, groupController.getReportedMessage);
router.get('/reports', validateAcc, isAdmin, groupController.getReportedMssages);

module.exports = router;
