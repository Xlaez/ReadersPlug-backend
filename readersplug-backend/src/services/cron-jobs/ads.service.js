/* eslint-disable no-param-reassign */
const { schedule } = require('node-cron');
const moment = require('moment');
const Ad = require('../../models/ad.model');

const runAdCheck = () =>
  schedule('0 0 */8 * * *', async () => {
    const today = moment().format('YYYY-MM-DD hh:mm:ss');
    const ads = await Ad.find({ expired: false });
    if (ads) {
      ads.forEach(async (ad) => {
        const dueDate = moment(ad.expiresAt).format('YYYY-MM-DD hh:mm:ss');
        if (moment(today).isSameOrAfter(moment(dueDate), 'day')) {
          ad.expired = true;
          await ad.save();
          // Update ad oder
        }
      });
    }
  });
module.exports = runAdCheck;
