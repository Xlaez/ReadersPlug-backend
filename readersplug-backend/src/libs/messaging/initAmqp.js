/* eslint-disable no-console */
/* eslint-disable no-promise-executor-return */
/* eslint-disable no-await-in-loop */
const amp = require('amqplib');
const { amqp } = require('../../config/config');
const logger = require('../../config/logger');

class MsgFactory {
  constructor() {
    this.amqp = amp;
    this.channel = this.config;
  }

  getChannel() {
    return this.channel;
  }

  async config() {
    setTimeout(() => {}, 15000);
    let conn;
    let retries = 5;

    while (retries > 0) {
      try {
        conn = await this.amqp.connect(amqp.uri);
        logger.info(`'RabbitMQ' connected successfuuly`);
        break;
      } catch (e) {
        retries -= 1;
        logger.debug(`'RabbitMQ' connection failed.  'retries' :`, e);
        await new Promise((res) => setTimeout(res, 5000));
      }
    }
    const channel = await conn.createChannel();
    return channel;
  }
}

module.exports = MsgFactory;
