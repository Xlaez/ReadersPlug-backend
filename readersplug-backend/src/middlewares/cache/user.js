const catchAsync = require('../../utils/catchAsync');
const { getFromRedis } = require('../../libs/redis');

const cacheUser = catchAsync(async (req, res, next) => {
  const { _id } = req.user;
  const data = await getFromRedis(_id.toString());
  if (data !== null) return res.status(200).send(JSON.parse(data));
  next();
});

const cacheMostFollowedUsers = catchAsync(async (req, res, next) => {
  const data = await getFromRedis('most-followed-users');
  if (data !== null) return res.status(200).send(JSON.parse(data));
  next();
});

module.exports = {
  cacheUser,
  cacheMostFollowedUsers,
};
