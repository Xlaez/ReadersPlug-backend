const express = require('express');
// const auth = require('../../../middlewares/auth');
const validate = require('../../../middlewares/validate');
const bookValidation = require('../../../validations/book.validation');
const bookController = require('../../../controllers/books/book.controller');
const commentController = require('../../../controllers/books/comment.controller');
const validateAcc = require('../../../middlewares/validateUser');
const { singleUpload } = require('../../../libs/multer');
const { libraryController } = require('../../../controllers');
const { isWriter } = require('../../../middlewares/roles');

const router = express.Router();

router.post('/', validateAcc, isWriter, singleUpload, validate(bookValidation.createBook), bookController.createBook);

router
  .route('/')
  .get(validateAcc, bookController.getBooks)
  .patch(validateAcc, validate(bookValidation.updateProfile), bookController.updateProfile);

router.get('/by-title/:name', bookController.getBookByTitle);
router.get('/search-by-title/:name', bookController.queryBookByTitle);
router.get('/by-category', bookController.queryBookByCategories);
router
  .route('/by-id/:bookId')
  .get(validateAcc, validate(bookValidation.getBook), bookController.getBookById)
  .patch(validateAcc, validate(bookValidation.updateBook), bookController.updateBook)
  .delete(validateAcc, validate(bookValidation.deleteBook), bookController.deleteBook);

router.get('/likes', bookController.getLikesAndDislikes);
router.patch('/:bookId/mark-completed', validateAcc, bookController.markBookCompleted);
router.patch('/:bookId/publish', validateAcc, bookController.publishBook);
router.put('/:bookId/like', validateAcc, bookController.likedBook);
router.purge('/:bookId/dislike', validateAcc, bookController.dislikeBook);
router.put('/:bookId/view', validateAcc, bookController.viewBook);
router.put('/:id/share', validateAcc, bookController.incrementShareNo);
router.patch('/:id/rate', validateAcc, bookController.incrementRating);

// comments
router.route('/comment').post(validateAcc, commentController.postComment).get(validateAcc, commentController.getComments);
router
  .route('/comment/:id')
  .get(validateAcc, commentController.getComment)
  .patch(validateAcc, commentController.updateComment)
  .delete(validateAcc, commentController.deleteComment);

router.get('/comment/:id/likes', validateAcc, commentController.getLikesAndDislikes);
router.put('/comment/:id/like', validateAcc, commentController.likeComment);
router.purge('/comment/:id/dislike', validateAcc, commentController.dislikeComment);

// chapters
router.get('/query-chapters', validateAcc, validate(bookValidation.queryChapters), bookController.queryBookChapters);
router.get(
  '/get-chapter-by-no',
  validateAcc,
  validate(bookValidation.queryChaptersByNo),
  bookController.queryBookChaptersByNo
);
router.delete('/chapter/:chapterId', validateAcc, bookController.deleteChapter);
router.get('/:bookId/chapter', validateAcc, bookController.getChapter);
router.get('/:bookId/chapters', validateAcc, bookController.getChaptersForABook);

router.post(
  '/:bookId/chapter',
  singleUpload,
  validateAcc,
  isWriter,
  validate(bookValidation.chapter),
  bookController.addChapter
);

//= =============================//
// LIBRARY ROUTES
//= ============================//

router.post('/library', validateAcc, validate(bookValidation.addTolibrary), libraryController.addBookToLibrary);
router.get('/library', validateAcc, validate(bookValidation.getBooksInLibrary), libraryController.getBooksInLibrary);
router.delete('/library/:id', validateAcc, libraryController.removeBookFromLibrary);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Books
 *   description: Book management and retrieval
 */

/**
 * @swagger
 * /books:
 *   post:
 *     summary: Create a book
 *     description: Only admins can create other books.
 *     tags: [Books]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - email
 *               - password
 *               - role
 *             properties:
 *               name:
 *                 type: string
 *               email:
 *                 type: string
 *                 format: email
 *                 description: must be unique
 *               password:
 *                 type: string
 *                 format: password
 *                 minLength: 8
 *                 description: At least one number and one letter
 *               role:
 *                  type: string
 *                  enum: [book, admin]
 *             example:
 *               name: fake name
 *               email: fake@example.com
 *               password: password1
 *               role: book
 *     responses:
 *       "201":
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Book'
 *       "400":
 *         $ref: '#/components/responses/DuplicateEmail'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *
 *   get:
 *     summary: Get all books
 *     description: Only admins can retrieve all books.
 *     tags: [Books]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: name
 *         schema:
 *           type: string
 *         description: Book name
 *       - in: query
 *         name: role
 *         schema:
 *           type: string
 *         description: Book role
 *       - in: query
 *         name: sortBy
 *         schema:
 *           type: string
 *         description: sort by query in the form of field:desc/asc (ex. name:asc)
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *           minimum: 1
 *         default: 10
 *         description: Maximum number of books
 *       - in: query
 *         name: page
 *         schema:
 *           type: integer
 *           minimum: 1
 *           default: 1
 *         description: Page number
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 results:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Book'
 *                 page:
 *                   type: integer
 *                   example: 1
 *                 limit:
 *                   type: integer
 *                   example: 10
 *                 totalPages:
 *                   type: integer
 *                   example: 1
 *                 totalResults:
 *                   type: integer
 *                   example: 1
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 * /books/{id}:
 *   get:
 *     summary: Get a book
 *     description: Logged in books can fetch only their own book information. Only admins can fetch other books.
 *     tags: [Books]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Book id
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Book'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   patch:
 *     summary: Update a book
 *     description: Logged in books can only update their own information. Only admins can update other books.
 *     tags: [Books]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Book id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               email:
 *                 type: string
 *                 format: email
 *                 description: must be unique
 *               password:
 *                 type: string
 *                 format: password
 *                 minLength: 8
 *                 description: At least one number and one letter
 *             example:
 *               name: fake name
 *               email: fake@example.com
 *               password: password1
 *     responses:
 *       "200":
 *         description: OK
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Book'
 *       "400":
 *         $ref: '#/components/responses/DuplicateEmail'
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 *
 *   delete:
 *     summary: Delete a book
 *     description: Logged in books can delete only themselves. Only admins can delete other books.
 *     tags: [Books]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: Book id
 *     responses:
 *       "200":
 *         description: No content
 *       "401":
 *         $ref: '#/components/responses/Unauthorized'
 *       "403":
 *         $ref: '#/components/responses/Forbidden'
 *       "404":
 *         $ref: '#/components/responses/NotFound'
 */
