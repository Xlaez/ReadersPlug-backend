const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const modelNames = require('../constants/modelNames.constants');

const notificationSchema = new Schema(
  {
    image: {
      type: String,
      enum: ['thumb', 'user', 'book', 'community', 'chat'], // this  would be updated  gradually
    },
    message: {
      type: String,
      required: true,
      minLength: 1,
    },
    link: {
      type: String,
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
    isSeen: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    toJSON: {
      virtuals: true,
    },
  }
);

notificationSchema.plugin(mongoosePaginate);

notificationSchema.methods.seen = function () {
  this.isSeen = true;
  return this.save;
};

const notifyModel = model(modelNames.notification, notificationSchema);

module.exports = notifyModel;
