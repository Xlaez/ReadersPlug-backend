const httpStatus = require('http-status');
const catchAsync = require('../../utils/catchAsync');
const { groupReportService } = require('../../services');
const ApiError = require('../../utils/ApiError');
const pick = require('../../utils/pick');

const makeReport = catchAsync(async (req, res, next) => {
  const { body, user } = req;

  const result = await groupReportService.createReport(user._id, body.msgId, body.groupId, body.reason);

  if (!result) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot make a report now'));

  res.status(201).send('reported');
});

const getReports = catchAsync(async (req, res, next) => {
  const options = pick(req.query, ['limit', 'page']);
  const result = await groupReportService.getAllReports({}, options);
  if (!result) return next(new ApiError(httpStatus.NOT_FOUND, 'resources not found'));
  res.status(200).send(result);
});

const getReport = catchAsync(async (req, res, next) => {
  const report = await groupReportService.getOneReport(req.params.id);
  if (!report) return next(new ApiError(httpStatus.NOT_FOUND, 'resource not found'));
  res.status(200).send(report);
});

const deleteReport = catchAsync(async (req, res, next) => {
  const result = await groupReportService.deleteReport(req.params.id);
  if (result.deletedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot delete report'));
  res.status(200).send('deleted');
});

module.exports = {
  makeReport,
  getReports,
  getReport,
  deleteReport,
};
