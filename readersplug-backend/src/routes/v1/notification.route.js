/** @format */
const express = require('express');
// eslint-disable-next-line no-unused-vars
const auth = require('../../middlewares/auth');
const validateAcc = require('../../middlewares/validateUser');
const { notificationController } = require('../../controllers');

const router = express.Router();

router.get('/', validateAcc, notificationController.getAllNotifiction);
router.get('/:notificationId', validateAcc, notificationController.getOneNotification);
router.delete('/:notificationId', validateAcc, notificationController.deleteOneNotification);
router.patch('/seen', validateAcc, notificationController.setNotificationsSeen);

module.exports = router;
