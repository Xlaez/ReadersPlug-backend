const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');

const isAdmin = (req, res, next) => {
  if (req.user.role !== 'admin') return next(new ApiError(httpStatus.UNAUTHORIZED, 'only admins can make this request'));
  next();
};

const isWriter = (req, res, next) => {
  if (req.user.role !== 'writer' || req.user.role !== 'both')
    return next(new ApiError(httpStatus.UNAUTHORIZED, 'only authors can make this request'));
  next();
};

module.exports = { isAdmin, isWriter };
