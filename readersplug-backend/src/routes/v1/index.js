const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const convRoute = require('./conversation.route');
const bookRoute = require('./books/book.route');
const groupRoute = require('./group.route');
const noteRoute = require('./note.route');
const docsRoute = require('./docs.route');
const communityRoute = require('./community/community.route');
const transactionRoute = require('./transaction.route');
const utilRoute = require('./utils.route');
// const notificationRoute = require('./notification.route');
const reportRoute = require('./report.route');
const adRoute = require('./ad.route');
const adminRoute = require('./admin.route');

const config = require('../../config/config');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/users',
    route: userRoute,
  },
  {
    path: '/admin',
    route: adminRoute,
  },
  {
    path: '/convs',
    route: convRoute,
  },
  {
    path: '/books',
    route: bookRoute,
  },
  {
    path: '/groups',
    route: groupRoute,
  },
  {
    path: '/community',
    route: communityRoute,
  },
  {
    path: '/notes',
    route: noteRoute,
  },
  {
    path: '/transaction',
    route: transactionRoute,
  },
  {
    path: '/util',
    route: utilRoute,
  },
  //   {
  //     path: '/notification',
  //     route: notificationRoute,
  //   },
  {
    path: '/report',
    route: reportRoute,
  },
  {
    path: '/ad',
    route: adRoute,
  },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
