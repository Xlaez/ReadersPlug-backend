// eslint-disable-next-line no-unused-vars
const myCustomLabels = require('../../utils/labelPaginate');

const { Library } = require('../../models');

const isBookInLibrary = async ({ bookId, userId, type }) => {
  const book = await Library.findOne({ $and: [{ book: bookId }, { userId }, { type }] });
  return book;
};

/**
 *
 * @param {object} param
 * @returns Promise<Library>
 */
const addBookToLibrary = async ({ bookId, userId, page, type }) => {
  // checks if book exists in the specific part of  the library i.e ["shelve" or "safe"]
  const isBookPresent = await isBookInLibrary({ bookId, userId, type });

  if (isBookPresent) throw new Error(`book already ${type}`);

  // adds book to library
  const book = await Library.create({
    book: bookId,
    page,
    type,
    userId,
  });

  return book;
};

const updateShelevBookNo = async (_id, no) => {
  const book = await Library.updateOne({ _id }, { $inc: { page: no } });
  return book;
};

const queryBooks = async (filter, options) => {
  const books = await Library.paginate(filter, options);
  return books;
};

const removeBookFromShelve = async (id) => {
  const book = await Library.deleteOne({ _id: id });
  return book;
};

module.exports = {
  addBookToLibrary,
  updateShelevBookNo,
  queryBooks,
  removeBookFromShelve,
};
