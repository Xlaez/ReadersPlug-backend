const { Schema, model } = require('mongoose');
const aggregatePagination = require('mongoose-aggregate-paginate-v2');
const pagination = require('mongoose-paginate-v2');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const readBy = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
  },
  {
    _id: false,
    timestamps: true,
  }
);

const schema = new Schema(
  {
    groupId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.chat_group,
      required: true,
    },
    message: {
      type: Schema.Types.Mixed,
    },
    sender: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
    readBy: [readBy],
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

schema.plugin(pagination);
schema.plugin(aggregatePagination);
schema.plugin(toJSON);

const message = model(modelNames.chat_groups_msg, schema);

module.exports = message;
