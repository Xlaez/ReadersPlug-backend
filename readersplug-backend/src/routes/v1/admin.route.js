const express = require('express');
const validate = require('../../middlewares/validate');
const validateAcc = require('../../middlewares/validateUser');
const { adminController } = require('../../controllers');
const { adminValidation } = require('../../validations');
const { isAdmin } = require('../../middlewares/roles');

const router = express.Router();

router.post('/suspend-user', validateAcc, isAdmin, validate(adminValidation.suspendUser), adminController.suspendUser);
router.post('/unsuspend-user', validateAcc, isAdmin, validate(adminValidation.unSuspendUser), adminController.unSuspendUser);
router.get(
  '/suspended-users',
  validateAcc,
  isAdmin,
  validate(adminValidation.getSuspendedUsers),
  adminController.getSuspendedAccounts
);

module.exports = router;
