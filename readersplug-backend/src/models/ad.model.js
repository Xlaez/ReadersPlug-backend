const { Schema, model } = require('mongoose');
const paginate = require('mongoose-paginate-v2');
const modelNames = require('../constants/modelNames.constants');

const schema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    content: {
      type: Schema.Types.Mixed, // combine text and image url here
      required: true,
    },
    // use ownerId over userId because in the future organisations and communities can post ads
    ownerId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
    locations: {
      type: [String],
      default: ['everywhere'],
      enum: ['everywhere', 'south-america', 'north-america', 'africa', 'asia', 'europe'], //update enum later
    },
    expiresAt: {
      type: Date,
      required: true,
    },
    type: {
      type: String,
      default: 'normal',
      enum: ['normal', 'premium'],
    },
    expired: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

schema.plugin(paginate);

const Ad = model(modelNames.ad, schema);
module.exports = Ad;
