const { model, Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const likeSchema = require('../schemas/likes.schema');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const commentSchema = Schema(
  {
    author: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
    bookId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.book,
      required: true,
    },
    content: {
      type: Schema.Types.Mixed,
      required: true,
    },
    likes: [likeSchema],
    dislikes: [likeSchema],
    totalLikes: {
      type: Number,
      default: 0,
    },
    totalDislikes: {
      type: Number,
      default: 0,
    },
    parentId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.book_comment,
      default: null,
    },
    totalReplies: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
commentSchema.plugin(mongoosePaginate);
commentSchema.plugin(toJSON);

commentSchema.statics.hasUserLiked = async function (commentId, userId) {
  const result = await this.findOne({ $and: [{ _id: commentId }, { likes: { $in: { userId } } }] });
  return !!result;
};

commentSchema.statics.hasUserDisLiked = async function (commentId, userId) {
  const result = await this.findOne({ $and: [{ _id: commentId }, { dislikes: { $in: { userId } } }] });
  return !!result;
};

/**
 * @typedef Comment
 */
const Comment = model(modelNames.book_comment, commentSchema);

module.exports = Comment;
