const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { bookCommentService } = require('../../services');
const Comment = require('../../models/books/comment.model');
const pick = require('../../utils/pick');
const { MESSAGES } = require('../../constants/responseMessages');

const postComment = catchAsync(async (req, res, next) => {
  const comment = await bookCommentService.createComment(req.body.bookId, req.body.content, req.user._id, req.body.parentId);
  if (!comment) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL));
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const getComments = catchAsync(async (req, res, next) => {
  const { limit, bookId, page, orderBy, sortedBy } = req.query;
  const comments = await bookCommentService.queryComments({ bookId, limit, page, orderBy, sortedBy });
  if (!comments) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING));
  res.status(200).json(comments);
});

const updateComment = catchAsync(async (req, res, next) => {
  const comment = await bookCommentService.updateComment(req.params.id, req.body.content);
  if (comment.modifiedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UPDATE_FAILED));
  res.status(200).json({ message: MESSAGES.UPDATED });
});

const deleteComment = catchAsync(async (req, res, next) => {
  const comment = await bookCommentService.deleteComment(req.params.id);
  if (comment.deletedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.DELETE_FAILED));
  res.status(200).json({ message: MESSAGES.DELETED });
});

const getComment = catchAsync(async (req, res, next) => {
  const comment = await bookCommentService.getComment(req.params.id);
  if (comment.deletedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.RESOURCE_MISSING));
  res.status(200).json(comment);
});

const likeComment = catchAsync(async (req, res) => {
  if (await Comment.hasUserDisLiked(req.params.id, req.user._id)) {
    await bookCommentService.updateComment(req.params.id, {
      $inc: { totalDislikes: -1, totalLikes: 1 },
      $pull: { dislikes: { userId: req.user._id } },
      $addToSet: { likes: { userId: req.user._id } },
    });
  } else if (await Comment.hasUserLiked(req.params.id, req.user._id)) {
    await bookCommentService.updateComment(req.params.id, {
      $inc: { totalLikes: -1 },
      $pull: { likes: { userId: req.user._id } },
    });
  } else {
    await bookCommentService.updateComment(req.params.id, {
      $inc: { totalLikes: 1 },
      $addToSet: { likes: { userId: req.user._id } },
    });
  }
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const dislikeComment = catchAsync(async (req, res) => {
  if (await Comment.hasUserLiked(req.params.id, req.user._id)) {
    await bookCommentService.updateComment(req.params.id, {
      $inc: { totalLikes: -1, totalDislikes: 1 },
      $pull: { likes: { userId: req.user._id } },
      $addToSet: { dislikes: { userId: req.user._id } },
    });
  } else if (await Comment.hasUserDisLiked(req.params.id, req.user._id)) {
    await bookCommentService.updateComment(req.params.id, {
      $inc: { totalDislikes: -1 },
      $pull: { dislikes: { userId: req.user._id } },
    });
  } else {
    await bookCommentService.updateComment(req.params.id, {
      $inc: { totalDislikes: 1 },
      $addToSet: { dislikes: { userId: req.user._id } },
    });
  }
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const getLikesAndDislikes = catchAsync(async (req, res) => {
  const options = pick(req.query, ['limit', 'page']);
  options.populate = [
    { path: 'likes.userId', select: 'author username _id' },
    { path: 'dislikes.userId', select: 'author username _id' },
  ];
  options.select = ['likes', 'dislikes', 'totalLikes', 'totalDislikes'];
  const result = await bookCommentService.getLikesAndDislikes({ _id: req.params.id }, options);
  if (!result) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(result);
});

module.exports = {
  postComment,
  getComment,
  getComments,
  updateComment,
  deleteComment,
  likeComment,
  dislikeComment,
  getLikesAndDislikes,
};
