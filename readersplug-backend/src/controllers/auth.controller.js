const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { authService, userService, tokenService, emailService } = require('../services');
const { addToRedisForCaching } = require('../libs/redis');
const ApiError = require('../utils/ApiError');
const { MESSAGES } = require('../constants/responseMessages');

const register = catchAsync(async (req, res) => {
  const user = await userService.createUser({ ...req.body, dob: req.body.birthday });
  await authService.getVeificationCode(req, user);
  res.status(httpStatus.CREATED).json({ user });
});

const setUsernameAndPassword = catchAsync(async (req, res, next) => {
  const { username, password, email } = req.body;
  const updateData = {
    username: `@${username}`,
    password,
  };
  const updatedUser = await userService.updateUserByEmail(email, updateData);
  if (!updatedUser) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.CANNOT_SET_PASSWORD));
  res.status(httpStatus.OK).json({ message: MESSAGES.PASSWORD_SET });
});

const resendVerficationCode = catchAsync(async (req, res) => {
  const user = await userService.getUserByEmail(req.body.email);
  if (!user) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.USER_NOT_FOUND);
  await authService.getVeificationCode(req, user);
  res.status(httpStatus.OK).json({ message: MESSAGES.SEND_VERIFICATION_CODE });
});

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(email, password);
  const tokens = await tokenService.generateAuthTokens(user);
  res.json({ user, tokens });
});

const logout = catchAsync(async (req, res) => {
  await authService.logout(req.body.refreshToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const refreshTokens = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refreshToken);
  res.status(httpStatus.OK).json({ ...tokens });
});

const forgotPassword = catchAsync(async (req, res) => {
  const resetPasswordToken = await tokenService.generateResetPasswordToken(req.body.email);
  await emailService.sendResetPasswordEmail(req.body.email, resetPasswordToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const resetPassword = catchAsync(async (req, res) => {
  await authService.resetPassword(req.query.token, req.body.password);
  res.status(httpStatus.NO_CONTENT).send();
});

const sendVerificationEmail = catchAsync(async (req, res) => {
  const verifyEmailToken = await tokenService.generateVerifyEmailToken(req.user);
  await emailService.sendVerificationEmail(req.user.email, verifyEmailToken);
  res.status(httpStatus.NO_CONTENT).send();
});

const verifyEmail = catchAsync(async (req, res) => {
  await authService.verifyEmail(req.query.token);
  res.status(httpStatus.NO_CONTENT).json({ message: MESSAGES.EMAIL_VERIFIED });
});

const getMe = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.user.id);
  user.password = '';
  await addToRedisForCaching(req.user.id.toString(), JSON.stringify(user), 360);
  res.status(httpStatus.OK).json(user);
});

const verifyAccount = catchAsync(async (req, res) => {
  const { digits } = req.body;
  const user = await authService.verifyAccount(digits);
  if (!user) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.CODE_EXPIRED);
  const token = await tokenService.generateAuthTokens(user);
  res.status(httpStatus.OK).json(token);
});

// this other function updates password while user is loggedIn which is opposite to the one up
const updatePassword = catchAsync(async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const user = await userService.getUserById(req.user.id);
  const doesMatch = await user.isPasswordMatch(oldPassword);
  if (!doesMatch) throw new ApiError(httpStatus.BAD_REQUEST, MESSAGES.PASSWORD_NO_MATCH);
  req.user.password = newPassword;
  req.user.save();
  res.status(httpStatus.OK).json({ message: MESSAGES.UPDATE_PASSWORD });
});

module.exports = {
  register,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  sendVerificationEmail,
  verifyEmail,
  verifyAccount,
  getMe,
  setUsernameAndPassword,
  resendVerficationCode,
  updatePassword,
};
