const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { notificationService } = require('../services');
const { MESSAGES } = require('../constants/responseMessages');

const getAllNotifiction = catchAsync(async (req, res) => {
  // pass the user's id to the service to get all the user's notification
  const { limit, page, orderBy, sortedBy } = req.query;
  const notify = await notificationService.findAllNotification(req.user._id, limit, page, orderBy, sortedBy);
  if (!notify) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(notify);
});

const getOneNotification = catchAsync(async (req, res) => {
  // get notification by notification's id
  const notify = await notificationService.findOneNotification(req.params.notificationId);
  if (!notify) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(notify);
});

const deleteOneNotification = catchAsync(async (req, res) => {
  const result = await notificationService.deleteOne(req.params.notificationId);
  if (!result) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.DELETE_FAILED);
  res.status(200).json({ message: MESSAGES.DELETED });
});

const setNotificationsSeen = catchAsync(async (req, res) => {
  const { user } = req;
  const result = await notificationService.setNotificationAsSeen(user._id);
  if (result.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

/**
 * This code below is depreciated
 */
const updateNotificationIfSeen = catchAsync(async (req, res) => {
  const notify = await notificationService.updateNotification(req.params.notificationId, req.body);
  if (!notify) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.RESOURCE_MISSING);
  res.status(200).send('notification seen');
});

module.exports = {
  getAllNotifiction,
  getOneNotification,
  deleteOneNotification,
  updateNotificationIfSeen,
  setNotificationsSeen,
};
