import NewsLetterController from '../controllers/newsletter.controller';
import { Router } from '@dolphjs/core';

class NewsletterRoute {
  public path?: string = '/v1/newsletter';
  public router = Router();
  protected controller: NewsLetterController = new NewsLetterController();

  constructor() {
    this.Routes();
  }

  private Routes() {
    this.router.post(`${this.path}`, this.controller.sendNewsletter);
  }
}

export default NewsletterRoute;
