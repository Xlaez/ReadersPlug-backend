const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const modelNames = require('../../constants/modelNames.constants');

const schema = new Schema({
  reporterId: {
    type: Schema.Types.ObjectId,
    ref: modelNames.user,
    required: true,
  },
  reportedMessage: {
    type: Schema.Types.ObjectId,
    ref: modelNames.conversation_msg,
    required: true,
  },
  convId: {
    type: Schema.Types.ObjectId,
    ref: modelNames.conversation,
  },
  reason: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 100,
  },
});

schema.plugin(mongoosePaginate);

const convReports = model('Convreport', schema);
module.exports = convReports;
