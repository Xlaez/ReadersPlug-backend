import { Request, Response } from 'express';
import { AppRes, catchAsync, httpStatus } from '@dolphjs/core';
import UserService from '../services/users.service';
import pick from '../utils/pick';
import sendmail from '../services/email.service';
import { sendMailUsingOldEmail } from '../services/oldemail.service';

class UserController {
  private service: UserService = new UserService();

  public addUser = catchAsync(async (req: Request, res: Response) => {
    const isEmailTaken = await this.service.isEmailTaken(req.body.email);
    if (isEmailTaken) throw new AppRes(httpStatus.BAD_REQUEST, 'a user with this email has already subscribed');
    const result = await this.service.addUser(req.body.email);
    if (!result) throw new AppRes(httpStatus.INTERNAL_SERVER_ERROR, 'cannot add user');
    //await sendmail.userRegistered(req.body.email, 'Joined Waitlist');
    await sendMailUsingOldEmail(req.body.email);

    res.status(httpStatus.CREATED).send('added');
  });

  public getAllUsers = catchAsync(async (req: Request, res: Response) => {
    const options = pick(req.query, ['sortBy', 'limit', 'page']);
    const users = await this.service.getUsers({}, options);
    res.status(httpStatus.OK).send(users);
  });

  // public deleteUsers = catchAsync(async (req: Request, res: Response) => {
  //   const result = await this.service.deleteAllUsers();
  //   if (result.deletedCount === 0)
  //     throw new AppRes(httpStatus.INTERNAL_SERVER_ERROR, 'an error occured, cannot delete users');
  //   res.status(httpStatus.OK).send('deleted');
  // });
}

export default UserController;
