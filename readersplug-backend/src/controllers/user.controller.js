const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { userService, feedService, notificationService } = require('../services');
const { uploadSingle } = require('../libs/cloudinary');
const { notificationInfo } = require('../services');
const { addToRedisForCaching } = require('../libs/redis');
const { MESSAGES } = require('../constants/responseMessages');

const createUser = catchAsync(async (req, res) => {
  const user = await userService.createUser({ ...req.body, dob: req.body.birthday });
  res.status(httpStatus.CREATED).json(user);
});

const getUsers = catchAsync(async (req, res) => {
  const { search } = req.query;
  const filter = pick(req.query, ['role']);
  const result = await userService.queryUsers({ search, filter }, req.query);
  res.status(httpStatus.OK).json(result);
});

const getUser = catchAsync(async (req, res) => {
  const user = await userService.getUserById(req.params.userId);
  if (!user) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.USER_NOT_FOUND);
  user.password = '';
  res.status(httpStatus.OK).json(user);
});

const getUserByUsername = catchAsync(async (req, res) => {
  let { username } = req.params;
  if (username.at(0) !== '@') {
    username = `@${username}`;
  }
  const user = await userService.getUserByUsername(username);
  if (!user) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.USER_NOT_FOUND);
  res.status(httpStatus.OK).json(user);
});

const updateUser = catchAsync(async (req, res) => {
  const user = await userService.updateUserById(req.params.userId, req.body);
  res.status(httpStatus.OK).json({
    _id: user._id,
    username: user.username,
    updatedAt: user.updatedAt,
  });
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.deleteAccount({ userId: req.user.id, email: req.user.email, reason: req.body.reason });
  res.status(httpStatus.OK).json({ message: MESSAGES.DELETED });
});

const setInterests = catchAsync(async (req, res, next) => {
  req.user.interests = req.body.interests;
  const user = await req.user.save();
  if (user.interests.length === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UPDATE_FAILED));
  res.status(httpStatus.OK).json({ message: MESSAGES.UPDATED });
});

const updateProfile = catchAsync(async (req, res, next) => {
  const user = await userService.updateUserById(req.user.id, req.body);
  if (!user) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.USER_NOT_FOUND));
  res.status(httpStatus.OK).json({
    _id: user._id,
    username: user.username,
    updatedAt: user.updatedAt,
  });
});

const updateAvatar = catchAsync(async (req, res) => {
  if (!req.file) throw new ApiError(httpStatus.BAD_REQUEST, MESSAGES.PROVIDE_IMAGE);
  const { publicId, url } = await uploadSingle(req.file.path);

  req.user.avatar = {
    publicId,
    url,
  };
  await req.user.save();
  res.status(httpStatus.OK).json({ message: MESSAGES.SUCCESS });
});

const follow = catchAsync(async (req, res) => {
  const result = await userService.followUser(req.user._id, req.params.userId);
  if (result === false) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, result);

  // create a notification for the  user who's been followed
  await notificationService.createNotification({
    image: notificationInfo.notificationInfo.requestImage,
    message: `${req.user.username} ${notificationInfo.notificationInfo.followed}`,
    link: 'http',
    userId: req.params.userId,
  });

  res.status(httpStatus.OK).json({ message: MESSAGES.FOLLOWED });
});

const unfollow = catchAsync(async (req, res) => {
  const result = await userService.unfollow(req.user._id, req.params.userId);
  if (!result) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, result);
  res.status(httpStatus.OK).json({ message: MESSAGES.UNFOLLOWED });
});

const getUserFollowers = catchAsync(async (req, res) => {
  const filter = { followedUser: req.params.userId };
  const options = pick(req.query, ['limit', 'page']);
  options.populate = { path: 'followingUser', select: 'avatar name username' };
  options.select = ['-followedUser'];
  const followers = await userService.getUserFollowers(filter, options);
  res.status(httpStatus.OK).json(followers);
});

const getUserFollowings = catchAsync(async (req, res) => {
  const filter = { followingUser: req.params.userId };
  const options = pick(req.query, ['limit', 'page']);
  options.populate = { path: 'followedUser', select: 'avatar name username' };
  options.select = ['-followingUser'];
  const followings = await userService.getUserFollowings(filter, options);
  res.status(httpStatus.OK).json(followings);
});

const toggleMatureContents = catchAsync(async (req, res) => {
  await userService.toggleMatureContents(req.user);
  res.status(httpStatus.OK).json({ message: MESSAGES.UPDATED });
});

const changeDefaultCurrency = catchAsync(async (req, res) => {
  if (!req.body.currency) throw new ApiError(httpStatus.BAD_REQUEST, MESSAGES.PROVIDE_CURRENCY_FIELD);
  await userService.changeCurrency(req.user, req.body.currency);
  res.status(httpStatus.OK).json({ message: MESSAGES.UPDATED });
});

const getMostFollowedUsers = catchAsync(async (req, res) => {
  const { limit, page } = req.query;
  const users = await feedService.getMostFollowedUsers(req.user._id, { limit, page });
  if (!users) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  await addToRedisForCaching('most-followed-users', JSON.stringify(users), 60);
  res.status(httpStatus.OK).json(users);
});

const getMostPopulatedCommunities = catchAsync(async (req, res) => {
  const { limit, page } = req.query;
  const communities = await feedService.getMostPopulatedCommunities(req.user._id, { limit, page });
  if (!communities) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(httpStatus.OK).json(communities);
});

const getMostLikedBooks = catchAsync(async (req, res) => {
  const { limit, page } = req.query;
  const books = await feedService.getMostLovedBooks(limit, page);
  if (!books) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(httpStatus.OK).json(books);
});

const getPaidBooks = catchAsync(async (req, res) => {
  const { limit, page } = req.query;
  const books = await feedService.getCostliestBooks(limit, page);
  if (!books) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(httpStatus.OK).json(books);
});

const getFreeBooks = catchAsync(async (req, res) => {
  const { limit, page } = req.query;
  const books = await feedService.getFreeBooks(limit, page);
  if (!books) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(httpStatus.OK).json(books);
});

const getLatestBooks = catchAsync(async (req, res) => {
  const { limit, page } = req.query;
  const books = await feedService.getNewestBooks(limit, page);
  if (!books) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(httpStatus.OK).json(books);
});

const getBooksByUserInterest = catchAsync(async (req, res) => {
  const { user } = req;
  const { limit, page } = req.query;
  const _user = await userService.getUserById(user._id);
  const books = await feedService.getBooksByUserInterest(_user.interests, limit, page);
  if (!books) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(httpStatus.OK).json(books);
});

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
  updateProfile,
  follow,
  unfollow,
  getUserFollowers,
  getUserFollowings,
  updateAvatar,
  toggleMatureContents,
  changeDefaultCurrency,
  getMostFollowedUsers,
  getMostPopulatedCommunities,
  getMostLikedBooks,
  getPaidBooks,
  getFreeBooks,
  getLatestBooks,
  getUserByUsername,
  setInterests,
  getBooksByUserInterest,
};
