const httpStatus = require('http-status');
const catchAsync = require('../../utils/catchAsync');
const { utilService } = require('../../services');
const ApiError = require('../../utils/ApiError');
const { MESSAGES } = require('../../constants/responseMessages');

const searchEverywhere = catchAsync(async (req, res, next) => {
  const searchResult = await utilService.search(req.query.keyword);
  if (!searchResult) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING));
  res.status(200).json(searchResult);
});

const lookUpMeaning = catchAsync(async (req, res, next) => {
  const searchResult = await utilService.lookUp(req.query.word);
  if (!searchResult) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING));
  res.status(200).json(searchResult);
});

module.exports = {
  searchEverywhere,
  lookUpMeaning,
};
