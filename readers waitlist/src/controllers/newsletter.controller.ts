import { Request, Response } from 'express';
import { AppRes, catchAsync, httpStatus } from '@dolphjs/core';
import sendmail from '../services/email.service';
import NewsLetterService from '../services/newsletter.service';
import pick from '../utils/pick';

class NewsLetterController {
  private service: NewsLetterService = new NewsLetterService();

  public sendNewsletter = catchAsync(async (req: Request, res: Response) => {
    await sendmail.sendNewsletter('This is a test', 'Hello there user, you subscribed for our newsletter');
    res.status(200).send('email sent successfully');
  });
}

export default NewsLetterController;
