const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
// eslint-disable-next-line no-unused-vars
const { communityComment, notificationService, notificationInfo } = require('../../services');
// eslint-disable-next-line no-unused-vars
const { uploadMany, uploadSingle } = require('../../libs/cloudinary');
const { MESSAGES } = require('../../constants/responseMessages');

/**
 * TODO: implement likes and dsilike for commments in the future
 * retrieval for comment's replies
 */

const submitComment = catchAsync(async (req, res) => {
  const { user, body } = req;

  const comment = await communityComment.createComment(body.postId, body.content, user._id, body.parentId || null);

  if (comment.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);

  // implement this part later.....

  //    await notificationService.createNotification({
  //      image: notificationInfo.notificationInfo.commentImage,
  //      message: "implement another time",
  //      link: 'http', // would be updated later
  //      userId: parentId,
  //    });

  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const getComments = catchAsync(async (req, res) => {
  const { limit, page, sortedBy, orderBy } = req.query;
  const comments = await communityComment.queryComments({ postId: req.params.postId, limit, page, orderBy, sortedBy });
  if (!comments) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(comments);
});

const updateComment = catchAsync(async (req, res) => {
  const { content } = req.body;
  const { commentId } = req.params;
  if (!content) throw new ApiError(httpStatus.BAD_REQUEST, MESSAGES.PROVIDE_BODY);
  const comment = await communityComment.updateComment(content, commentId);
  if (comment.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UPDATE_FAILED);
  res.status(200).json({ message: MESSAGES.UPDATED });
});

const deleteComment = catchAsync(async (req, res) => {
  const { commentId } = req.params;
  const result = await communityComment.deleteComment(commentId);
  if (result.deletedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.DELETE_FAILED);
  res.status(200).json({ message: MESSAGES.DELETED });
});

module.exports = {
  submitComment,
  getComments,
  updateComment,
  deleteComment,
};
