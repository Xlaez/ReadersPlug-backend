const { SuspendedAcc } = require('../models');
const { userService } = require('.');

const suspendUser = async (userId, duration, reason) => {
  await userService.updateUserById(userId, { isSuspended: true });
  return SuspendedAcc.create({
    userId,
    duration,
    reason,
  });
};

const getSuspendedAccounts = async (filter, options) => {
  return SuspendedAcc.paginate(filter, options);
};

const unSuspendUser = async (userId) => {
  await userService.updateUserById(userId, { isSuspended: false });
  return SuspendedAcc.deleteOne({ userId });
};

module.exports = { suspendUser, getSuspendedAccounts, unSuspendUser };
