/* eslint-disable no-unused-vars */
const express = require('express');
const validate = require('../../middlewares/validate');
const { adValidation } = require('../../validations');
const { adController } = require('../../controllers');
const { singleUpload } = require('../../libs/multer');

// eslint-disable-next-line no-unused-vars
const auth = require('../../middlewares/auth');
const validateAcc = require('../../middlewares/validateUser');

const router = express.Router();

router.post('/', validateAcc, singleUpload, validate(adValidation.createAd), adController.createAd);
router.get('/query', validateAcc, validate(adValidation.getAds), adController.getAds);
router.get('/:id', validateAcc, adController.getAdById);
router.delete('/:id', validateAcc, adController.deleteAd);

module.exports = router;
