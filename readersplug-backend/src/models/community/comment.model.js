const { Schema, model } = require('mongoose');
const mongoosePagination = require('mongoose-paginate-v2');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const schema = new Schema({
  author: {
    type: String,
    required: true,
    ref: modelNames.user,
  },
  content: {
    type: String,
    required: true,
  },
  parentId: {
    type: Schema.Types.ObjectId,
    ref: modelNames.community_comment,
  },
  postId: {
    type: Schema.Types.ObjectId,
    ref: modelNames.community_posts,
    required: true,
  },
  replyCount: {
    type: Number,
    default: 0,
  },
  totalLikes: {
    type: Number,
    defaut: 0,
  },
  likes: [
    {
      userId: {
        type: Schema.Types.ObjectId,
        ref: modelNames.user,
      },
    },
  ],
});

schema.plugin(mongoosePagination);
schema.plugin(toJSON);

const commentSchema = model(modelNames.community_comment, schema);
module.exports = commentSchema;
