const express = require('express');
const validate = require('../../../middlewares/validate');
const { communityValidation } = require('../../../validations');
const { communityController, communityPostController, communityCommentController } = require('../../../controllers');
const { singleUpload, multipleUpload } = require('../../../libs/multer');
// eslint-disable-next-line no-unused-vars
const auth = require('../../../middlewares/auth');
const validateAcc = require('../../../middlewares/validateUser');

const router = express.Router();

router.post('/new', validateAcc, validate(communityValidation.createCommunity), communityController.createCommunity);
router.get('/', validateAcc, communityController.queryCommunities);
router.get('/by-id/:id', validateAcc, communityController.getCommunityById);
router.get('/by-name/:name', validateAcc, communityController.getCommunityByName);
router.get('/:id/members', validateAcc, communityController.getMembers);
router.get('/:id/requests', validateAcc, communityController.getRequests);
router.post('/:id/request', validateAcc, communityController.sendRequestToCommunity);
router.delete('/:id/reject-request', validateAcc, communityController.rejectRequest);
router.patch('/:communityId/:requestId/:userId/accept-request', validateAcc, communityController.acceptRequest);

router.put(
  '/:id/add-member',
  validateAcc,
  validate(communityValidation.joinOrLeaveCommunity),
  communityController.addMember
);
router.purge(
  '/:id/remove-member',
  validateAcc,
  validate(communityValidation.joinOrLeaveCommunity),
  communityController.removeMember
);
router.put('/:id/add-admin', validateAcc, validate(communityValidation.addOrRemoveAdmin), communityController.addAdmin);
router.purge(
  '/:id/remove-admin',
  validateAcc,
  validate(communityValidation.addOrRemoveAdmin),
  communityController.removeAdmin
);
router.patch('/:id/update-info', validateAcc, validate(communityValidation.updateInfo), communityController.updateInfo);
router.patch(
  '/:id/update-other',
  validateAcc,
  validate(communityValidation.updateRulesAndType),
  communityController.updateRulesAndType
);
router.patch('/:id/upload-image', validateAcc, singleUpload, communityController.uploadImage);
router.delete('/:id', validateAcc, communityController.deleteCommunity);

/**
 * FROM HERE INCLUDES ROUTES BELONGING TO COMMUNITY POSTS
 */

router.post(
  '/post',
  validateAcc,
  multipleUpload,
  validate(communityValidation.createPost),
  communityPostController.createPost
);
router.get('/post/by-id/:id', validateAcc, communityPostController.getPostById);
router.get('/:communityId/posts', validateAcc, communityPostController.queryPosts);
// this route gets comments total, likes and shares
router.get('/post/:id/likes', validateAcc, communityPostController.getPostLikes);
router.patch('/post/:id/like', validateAcc, communityPostController.likePost);
router.patch('/post/:id/unlike', validateAcc, communityPostController.unlikePost);

router.patch('/post/:id', validateAcc, validate(communityValidation.updatePost), communityPostController.updatePosts);
router.delete('/post/:id', validateAcc, communityPostController.deletePost);

router.post('/comment', validateAcc, communityCommentController.submitComment);
router.patch('/comment/:commentId', validateAcc, communityCommentController.updateComment);
router.delete('/comment/:commentId', validateAcc, communityCommentController.deleteComment);
router.get('/:postId/comments', validateAcc, communityCommentController.getComments);

router.post(
  '/:communityId/post/:postId/share',
  validateAcc,
  validate(communityValidation.sharePost),
  communityPostController.sharePost
);

module.exports = router;
