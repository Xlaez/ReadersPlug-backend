const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const modelNames = require('../../constants/modelNames.constants');

const schema = new Schema({
  reporterId: {
    type: Schema.Types.ObjectId,
    ref: modelNames.user,
    required: true,
  },
  reportedMsg: {
    type: Schema.Types.ObjectId,
    ref: modelNames.chat_groups_msg,
    required: true,
  },
  groupId: {
    type: Schema.Types.ObjectId,
    ref: modelNames.chat_group,
  },
  reason: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 100,
  },
});

schema.plugin(mongoosePaginate);

const groupReports = model('GroupReport', schema);
module.exports = groupReports;
