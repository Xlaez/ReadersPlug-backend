// eslint-disable-next-line no-unused-vars
const myCustomLabels = require('../../utils/labelPaginate');

const { Book, BookComment } = require('../../models');

const createComment = async (bookId, content, userId, parentId) => {
  // create a new comment document
  let newComment = new BookComment({
    author: userId,
    bookId,
    content,
  });

  // if parentId then find the comment and push current comment as a reply to the [replies] Array
  if (parentId) {
    const pCom = await BookComment.findOne({ _id: parentId });

    if (!pCom) throw new Error('parent comment not found');
    newComment.parentId = parentId;
    await BookComment.updateOne(
      { _id: parentId },
      {
        $inc: { totalReplies: 1 },
      }
    );
  }
  // save new comment body as a reply
  newComment = await newComment.save();

  // increment comment count on the book document
  return Book.updateOne(
    { _id: bookId },
    {
      $inc: { commentCount: 1 },
    }
  );
};

const queryComments = async ({ bookId, limit, page, orderBy, sortedBy }) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };

  const comments = await BookComment.paginate(
    {
      bookId,
    },
    {
      ...(limit ? { limit } : { limit: 20 }),
      page,
      sort: { [orderBy]: sortedBy === 'asc' ? 1 : -1 },
      ...options,
    }
  );

  return comments;
};

const updateComment = async (commentId, content) => {
  const comment = await BookComment.updateOne({ _id: commentId }, { content });
  return comment;
};

const deleteComment = async (commentId) => {
  const comment = await BookComment.findById(commentId);
  if (!comment) throw new Error('cannot find comment');
  const { parentId, bookId } = comment;
  const book = await Book.findById(bookId);
  const { commentCount } = book;

  if (commentCount < 1) throw new Error('no comments');

  // if parentId pull the comment from the parent comment's [replies] Array and reduce the [totalReplies]
  if (parentId) {
    await BookComment.findByIdAndUpdate(parentId, { $inc: { totalReplies: -1 } });
  }

  //   reduce book commentCount on docuemnt
  book.commentCount -= 1;
  await book.save();
  return BookComment.deleteOne({ _id: comment._id });
};

const getCommentById = async (id) => {
  return BookComment.findById(id);
};

const getLikesAndDislikes = async (filter, options) => {
  return BookComment.paginate(filter, options);
};

/**
 * TODO: paginate replies
 */
const getComment = async (id) => {
  const comment = await BookComment.findById(id).populate('author', 'avatar username _id').lean();
  const replies = await BookComment.find({ parentId: id }).lean();
  return { comment, replies };
};

module.exports = {
  createComment,
  queryComments,
  updateComment,
  deleteComment,
  getComment,
  getCommentById,
  getLikesAndDislikes,
};
