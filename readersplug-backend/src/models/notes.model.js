const { model, Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const modelNames = require('../constants/modelNames.constants');
const toJSON = require('./plugins/toJSON.plugin');

const schema = new Schema(
  {
    content: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

schema.plugin(mongoosePaginate);
schema.plugin(toJSON);

const noteModel = model(modelNames.note, schema);
module.exports = noteModel;
