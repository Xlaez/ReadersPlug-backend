const express = require('express');
const cors = require('cors');
const compression = require('compression');
const helmet = require('helmet');
const xss = require('xss-clean');
const mongoSanitize = require('express-mongo-sanitize');
const notificatiionRoute = require('../routes/v1/notification.route');

const app = express();

app.use(helmet());

// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// sanitize request data
app.use(xss());
app.use(mongoSanitize());

// gzip compression
app.use(compression());

app.use(
  cors({
    origin: '*',
  })
);

app.options('*', cors());

app.use('/v1/notification', notificatiionRoute);
