import { config } from 'dotenv';
config({});

/**
 * You can  export all your environmental variables from here
 */
export const {
  PORT,
  MONGO_URI,
  ENV,
  SEND_GRID_KEY,
  EMAIL_FROM,
  SMTP_PASS,
  SMTP_USER,
  FACEBOOK_LINK,
  LINKEDIN_LINK,
  TWITTER_LINK,
} = process.env;
