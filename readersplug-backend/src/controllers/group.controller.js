const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const ApiError = require('../utils/ApiError');
const { uploadSingle, uploadMany } = require('../libs/cloudinary');
const { groupService } = require('../services');
const pick = require('../utils/pick');
const { MESSAGES } = require('../constants/responseMessages');

/**
 * TODO: check if user leaving a group is an admin, if so then make a new user admin by default
 */

const createGroup = catchAsync(async (req, res) => {
  const { file, body } = req;

  let data = {
    members: body.members || [],
    admins: [req.user._id],
    name: body.name,
  };
  data.members.push(req.user._id);

  if (file) {
    const { publicId, url } = await uploadSingle(file.path);

    // eslint-disable-next-line prefer-const
    let fileData = {};
    Object.assign(fileData, data, { logo: { publicId, url } });
    data = fileData;
  }
  const result = await groupService.initiateGroup(data);
  if (!result) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(201).json(result);
});

const getGroupById = catchAsync(async (req, res) => {
  const group = await groupService.findGroupById(req.params.id);
  if (!group) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(group);
});

const getGroupByName = catchAsync(async (req, res) => {
  const group = await groupService.findGroupByName(req.params.name, req.user._id);
  if (!group) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(group);
});

const uploadLogo = catchAsync(async (req, res) => {
  if (!req.file) throw new ApiError(httpStatus.BAD_REQUEST, MESSAGES.PROVIDE_IMAGE);
  const { publicId, url } = await uploadSingle(req.file.path);

  const group = await groupService.updateGroupById(req.params.groupId, { logo: { url, publicId } });
  if (!group) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UPDATE_FAILED);
  res.status(200).json({ message: MESSAGES.UPDATED });
});

const deleteGroup = catchAsync(async (req, res) => {
  await groupService.deleteGroup(req.params.groupId);
  res.status(200).json({ message: MESSAGES.DELETED });
});

const sendMessage = catchAsync(async (req, res) => {
  const { groupId, text } = req.body;
  const { user, files } = req;

  let message;
  if (text && files.length) {
    const filePaths = files.map((file) => file.path);
    const response = await uploadMany(filePaths);
    const fileData = response.map((file) => ({ url: file.url, publicId: file.publicId }));

    message = { fileData, text };
  } else if (files.length) {
    const filePaths = files.map((file) => file.path);
    const response = await uploadMany(filePaths);
    const fileData = response.map((file) => ({ url: file.url, publicId: file.publicId }));

    message = { fileData };
  } else if (text) {
    message = { text };
  } else throw ApiError(httpStatus.BAD_REQUEST, MESSAGES.PROVIDE_BODY);

  const msg = await groupService.postMessage({ message, groupId, sender: user._id, readBy: { userId: user._id } });
  res.status(201).json(msg);
});

const addMembers = catchAsync(async (req, res) => {
  const { body, params } = req;
  const group = await groupService.addMembers(params.groupId, body.members);
  if (!group) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const addAdmins = catchAsync(async (req, res) => {
  const { body, params } = req;
  const group = await groupService.addAdmins(params.groupId, body.admins);
  if (!group) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const removeMembers = catchAsync(async (req, res) => {
  const { body, params } = req;
  const group = await groupService.removeMembers(params.groupId, body.members);
  if (!group) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const removeAdmins = catchAsync(async (req, res) => {
  const { body, params } = req;
  const group = await groupService.removeAdmins(params.groupId, body.admins);
  if (!group) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const getMessagesByGroupId = catchAsync(async (req, res) => {
  const filter = pick(req.params, ['groupId']);
  const options = pick(req.query, ['limit', 'page']);
  options.populate = [
    { path: 'sender', select: 'username name avatar' },
    { path: 'readBy.userId', select: 'username name' },
  ];
  const msgs = await groupService.getMsgsByGroupId(filter, options);
  if (!msgs) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(msgs);
});

const markMessagesSeen = catchAsync(async (req, res) => {
  const msg = await groupService.markMessageRead(req.params.groupId, req.user._id);
  if (!msg) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const deleteMessage = catchAsync(async (req, res) => {
  await groupService.markMsgDeleted(req.params.msgId);
  res.status(200).json({ message: MESSAGES.DELETED });
});

const getGoupsRecentMsgs = catchAsync(async (req, res) => {
  const groups = await groupService.getGroupsByUseId(req.user._id);
  if (!groups.length) throw new ApiError(httpStatus.NOT_FOUND, 'user is in no group');

  const groupIds = groups.map((group) => group._id);
  const recentConversations = await groupService.getRecentGroupMsgs(groupIds, req.user._id);
  const msgs = recentConversations.map((group) => {
    const membersProfile = group.groupdata.membersProfile.flat();
    const profile = membersProfile.map((user) => {
      return {
        id: user._id,
        username: user.username,
        name: user.name,
        gender: user.gender,
        avatar: user.avatar,
      };
    });
    const seenBy = group.readBy.flat();

    return {
      id: group._id,
      msgId: group.msgId,
      groupId: group.groupId,
      sender: group.sender,
      msg: group.msg,
      createdAt: group.createdAt,
      unread: group.unread,
      seenBy,
      membersProfile: profile,
    };
  });
  res.status(200).json(msgs);
});

const reportMessage = catchAsync(async (req, res) => {
  const { msgId, groupId, reason } = req.body;
  const report = await groupService.reportMessage(req.user._id, msgId, groupId, reason);
  if (!report) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const getReportedMessage = catchAsync(async (req, res) => {
  const reportedMsgs = await groupService.getReportedMsg(req.params.reportedId);
  if (!reportedMsgs) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(reportedMsgs);
});

const getReportedMssages = catchAsync(async (req, res) => {
  const reportedMsgs = await groupService.getReportedMessages();
  if (!reportedMsgs) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(reportedMsgs);
});

module.exports = {
  createGroup,
  getGroupById,
  getGroupByName,
  uploadLogo,
  deleteGroup,
  sendMessage,
  addMembers,
  addAdmins,
  removeAdmins,
  removeMembers,
  getMessagesByGroupId,
  markMessagesSeen,
  deleteMessage,
  getGoupsRecentMsgs,
  reportMessage,
  getReportedMessage,
  getReportedMssages,
};
