import random
import time
import traceback
import requests

user_email = input("Input the user's email \n")
user_pass = input("Input the user's password \n")
user_id = input("Input the user to follow's id \n") 

data = {
    "email": user_email,
    "password": user_pass
}

login_response = requests.post(f"http://localhost:8080/v1/auth/login", verify=False, data=data)

if login_response.status_code == 200:
    user_follow_request = requests.get(f"http://localhost:8080/api/v1/users/received-requests/{user_id}", verify=False, headers={"x-auth-token": login_response.json()["tokens"]["access"]["token"]})
    if user_follow_request.status_code != 200:
        print(user_follow_request.json())
    else:
        ans = user_follow_request.json()
        for rows in ans:
            id = rows["id"]
            accept_follow_response = requests.patch(f"http://localhost:8080/v1/users/follow/accept/{id}", verify=False, headers={"x-auth-token": login_response.json()["tokens"]["access"]["token"]})
            print(accept_follow_response.status_code)
else:
    # print("An error occured while trying to login user")
    print(login_response.json())