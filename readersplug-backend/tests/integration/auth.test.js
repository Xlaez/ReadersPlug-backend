/* eslint-disable no-unused-vars */
const request = require('supertest');
const faker = require('faker');
const httpStatus = require('http-status');
const { User, Token } = require('../../src/models');
// const setupTestDB = require('../utils/setupTestDB');
const app = require('../../src/servers/app');

// setupTestDB();

describe('Auth routes', () => {
  describe('POST /v1/auth/register', () => {
    let newUser;
    beforeEach(() => {
      newUser = {
        name: faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        role: 'both',
        phoneNumber: +2337191082611,
        gender: 'female',
        birthday: new Date('08-10-2001'),
      };
    });

    test('should return 201 status code and successfully register user', async () => {
      await request(app).post('/v1/auth/register').send(newUser).expect(httpStatus.CREATED);
    });
  });
});
