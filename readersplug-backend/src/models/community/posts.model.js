const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const likeSchema = require('../schemas/likes.schema');
const shareSchema = require('../schemas/share.schema');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const schema = new Schema(
  {
    content: {
      type: String,
      minlength: 1,
    },
    communityId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.community,
      required: true,
    },
    file: {
      type: Schema.Types.Mixed,
    },
    commentCount: {
      type: Number,
      default: 0,
    },
    author: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
    likes: [likeSchema],
    shares: [shareSchema],
    totalLikes: {
      type: Number,
      default: 0,
    },
    totalShares: {
      type: Number,
      default: 0,
    },
    // this is the id for a shared post
    sharedPostId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.community_posts,
    },
  },
  {
    timestamps: true,
  }
);

schema.plugin(mongoosePaginate);
schema.plugin(toJSON);

schema.statics.hasUserLiked = async function (commentId, userId) {
  const result = await this.findOne({ $and: [{ _id: commentId }, { likes: { $in: { userId } } }] });
  return !!result;
};

schema.statics.hasUserDisLiked = async function (commentId, userId) {
  const result = await this.findOne({ $and: [{ _id: commentId }, { dislikes: { $in: { userId } } }] });
  return !!result;
};

const postModel = model(modelNames.community_posts, schema);
module.exports = postModel;
