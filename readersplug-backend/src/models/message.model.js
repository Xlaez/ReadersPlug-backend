const { Schema, model } = require('mongoose');
const { toJSON, paginate } = require('./plugins');
const modelNames = require('../constants/modelNames.constants');

const readBySchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
    },
    createdAt: {
      type: Date,
      default: new Date(),
    },
  },
  {
    _id: false,
  }
);

const messageSchema = new Schema(
  {
    convId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.conversation,
      required: true,
    },
    message: {
      type: Schema.Types.Mixed,
      required: true,
    },
    sender: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    readBy: [readBySchema],
  },
  {
    timestamps: true,
  }
);

messageSchema.plugin(toJSON);
messageSchema.plugin(paginate);

const msgModel = model(modelNames.conversation_msg, messageSchema);

module.exports = msgModel;
