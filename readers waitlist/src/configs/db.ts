import { MONGO_URI } from './index';

const mongoConfig = {
  url: MONGO_URI,
  options: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoIndex: true,
    dbName: 'readers-waitlist',
  },
};

export default mongoConfig;
