const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const schema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: modelNames.user,
    required: true,
  },
  book: {
    type: Schema.Types.ObjectId,
    ref: modelNames.book,
    required: true,
  },
  liked: {
    type: Boolean,
    default: false,
  },
  page: {
    type: Number,
  },
  type: {
    type: String,
    enum: ['shelved', 'saved'],
    required: true,
  },
});

schema.plugin(mongoosePaginate);
schema.plugin(toJSON);

module.exports = model(modelNames.library, schema);
