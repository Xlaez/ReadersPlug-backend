const { Schema } = require('mongoose');
const modelNames = require('../../constants/modelNames.constants');

const shareSchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
    },
  },
  { _id: false }
);

module.exports = shareSchema;
