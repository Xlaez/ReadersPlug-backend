const mongoose = require('mongoose');
// const bcrypt = require('bcryptjs');
const faker = require('faker');
const User = require('../../src/models/user.model');

const password = 'password1';
// const salt = bcrypt.genSaltSync(8);
// const hashedPassword = bcrypt.hashSync(password, salt);

const userOne = {
  _id: mongoose.Types.ObjectId(),
  name: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  password,
  role: 'both',
  phoneNumber: +2337191082611,
  gender: 'female',
  birthday: new Date('08-10-2001'),
};

const userTwo = {
  _id: mongoose.Types.ObjectId(),
  name: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  password,
  role: 'reader',
  phoneNumber: +2347019107661,
  gender: 'female',
  birthday: new Date('12-06-2002'),
};

const admin = {
  _id: mongoose.Types.ObjectId(),
  name: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  password,
  role: 'admin',
  phoneNumber: +4121318890,
  gender: 'male',
  birthday: new Date('05-03-2000'),
};

const insertUsers = async (users) => {
  await User.insertMany(users.map((user) => ({ ...user })));
};

module.exports = {
  userOne,
  userTwo,
  admin,
  insertUsers,
};
