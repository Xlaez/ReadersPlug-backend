const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { libraryService } = require('../../services');
const { MESSAGES } = require('../../constants/responseMessages');

const addBookToLibrary = catchAsync(async (req, res, next) => {
  const book = await libraryService.addBookToLibrary({ ...req.body, userId: req.user._id });
  if (!book) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL));
  res.status(201).json({ message: MESSAGES.SUCCESS });
});

const getBooksInLibrary = catchAsync(async (req, res, next) => {
  const filter = pick(req.query, ['type', 'user']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const books = await libraryService.queryBooks(filter, options);
  if (!books) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING));
  res.status(200).json(books);
});

const removeBookFromLibrary = catchAsync(async (req, res, next) => {
  const result = await libraryService.removeBookFromShelve(req.params.id);
  if (result.deletedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.DELETE_FAILED));
  res.status(200).json({ message: MESSAGES.DELETED });
});

module.exports = {
  addBookToLibrary,
  getBooksInLibrary,
  removeBookFromLibrary,
};
