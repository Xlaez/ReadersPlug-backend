const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { convService } = require('../services');
const ApiError = require('../utils/ApiError');
const { uploadMany } = require('../libs/cloudinary');
const pick = require('../utils/pick');
const { MESSAGES } = require('../constants/responseMessages');

const sendMessage = catchAsync(async (req, res) => {
  const { user, body, files } = req;

  let conv;
  if (!body.convId) {
    conv = await convService.createNewConv(user._id, body.to);
    body.convId = conv._id;
  }
  conv = await convService.findById(body.convId);
  if (!conv || conv.blockedIds.includes(user._id)) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);

  // check type of message
  let payload;

  if (body.text && files.length) {
    const paths = files.map((file) => file.path);
    const result = await uploadMany(paths);
    const fileData = result.map((file) => ({ url: file.url, publicId: file.publicId }));
    payload = { text: body.text, fileData };
  } else if (body.text) {
    payload = { text: body.text };
  } else if (files.length) {
    const paths = files.map((file) => file.path);
    const result = await uploadMany(paths);
    const fileData = result.map((file) => ({ url: file.url, publicId: file.publicId }));
    payload = { fileData };
  } else {
    throw new ApiError(httpStatus.BAD_REQUEST, MESSAGES.PROVIDE_BODY);
  }

  const msg = await convService.postMessage({ sender: user._id, convId: conv._id }, payload);
  res.status(201).json(msg);
});

const getMessageByConvId = catchAsync(async (req, res) => {
  const filter = pick(req.params, ['convId']);
  const options = pick(req.query, ['limit', 'page']);
  //   options.populate = { path: 'sender', select: 'username avatar _id' };
  //   const members = await conversation.populate('members', 'avatar name username role');
  const msgs = await convService.getMsgByConvId(filter, options);
  if (!msgs) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(msgs);
});

const getRecentConversations = catchAsync(async (req, res) => {
  // get all user conversations
  const conversations = await convService.getConvsByUserId(req.user._id);
  if (!conversations) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  // get messages across all conversations
  const convIds = conversations.map((conv) => conv._id);
  const recentConversations = await convService.getRecentConversations(convIds, req.user._id);

  const flatconv = recentConversations.map((conv) => {
    const usersprofile = conv.convData.flat();
    const readBy = conv.readBy.flat();
    const profile = usersprofile.map((p) => {
      return {
        id: p._id,
        name: p.name,
      };
    });

    return {
      id: conv._id,
      msgId: conv.msgId,
      convId: conv.conId,
      msg: conv.msg,
      sender: conv.sender,
      readBy,
      createdAt: conv.createdAt,
      unread: conv.unread,
      isDeleted: conv.isDeleted,
      profile,
      blocked: conv.blocked,
    };
  });

  res.status(200).json(flatconv);
});

const deleteMsg = catchAsync(async (req, res) => {
  const msg = await convService.markDeleted(req.params.id);
  if (msg.modifiedCount !== 1 || !msg) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.DELETE_FAILED);
  res.status(200).json({ message: MESSAGES.DELETED });
});

const blockConversation = catchAsync(async (req, res) => {
  const conv = await convService.blockConversation(req.params.convId, req.params.userId);
  if (!conv || !conv.blocked) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const unBlockConversation = catchAsync(async (req, res) => {
  const conv = await convService.unBlockConversation(req.params.convId, req.params.userId);
  if (!conv) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const reportMessage = catchAsync(async (req, res) => {
  const { msgId, convId, reason } = req.body;
  const report = await convService.report(req.user._id, msgId, convId, reason);
  if (!report) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(201).json({ message: MESSAGES.SUCCESS });
});

const getAReport = catchAsync(async (req, res) => {
  const report = await convService.getAReport(req.params.reportId);
  if (!report) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(report);
});

/**
 * TODO: paginate and populate this service
 */
const getReports = catchAsync(async (req, res) => {
  const reports = await convService.getReports();
  if (!reports.length) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(reports);
});
module.exports = {
  sendMessage,
  getMessageByConvId,
  getRecentConversations,
  deleteMsg,
  blockConversation,
  unBlockConversation,
  reportMessage,
  getAReport,
  getReports,
};
