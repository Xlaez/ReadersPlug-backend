const Joi = require('joi');

const suspendUser = Joi.object().keys({
  userId: Joi.string().required(),
  reason: Joi.string().required(),
  duration: Joi.date().required(),
});

const unSuspendUser = Joi.object().keys({
  userId: Joi.string().required(),
});

const getSuspendedUsers = Joi.object().keys({
  limit: Joi.number().required(),
  page: Joi.number().required(),
  date: Joi.string(),
});

module.exports = { suspendUser, getSuspendedUsers, unSuspendUser };
