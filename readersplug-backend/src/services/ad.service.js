/* eslint-disable camelcase */
const { Ad } = require('../models');

const createAd = async (data) => {
  return Ad.create(data);
};

const getAdById = async (adId) => {
  return Ad.findById(adId).populate('ownerId', 'username avatar _id').lean();
};

const getAds = async (filter, options) => {
  return Ad.paginate(filter, options);
};

const deleteAd = async (ownerId, ad_id) => {
  return Ad.deleteOne({ $and: [{ ownerId }, { _id: ad_id }] });
};

module.exports = {
  createAd,
  getAdById,
  getAds,
  deleteAd,
};
