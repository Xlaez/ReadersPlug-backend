const express = require('express');
const helmet = require('helmet');
const xss = require('xss-clean');
const mongoSanitize = require('express-mongo-sanitize');
const compression = require('compression');
const cors = require('cors');
const passport = require('passport');
const httpStatus = require('http-status');
const expressSession = require('express-session');
// eslint-disable-next-line no-unused-vars
const cookieParser = require('cookie-parser');
const http = require('http');
const { default: mongoose } = require('mongoose');
const config = require('../config/config');
const morgan = require('../config/morgan');
const { jwtStrategy } = require('../config/passport');
const { authLimiter } = require('../middlewares/rateLimiter');
const routes = require('../routes/v1');
const { errorConverter, errorHandler } = require('../middlewares/error');
const ApiError = require('../utils/ApiError');
const { initPassport, facebookAuth, googleAuth } = require('../services/oauth.service');
const { getIo } = require('../socket');
const logger = require('../config/logger');
const { connectQueue } = require('../libs/messaging/rabbitMq-server');
const adCronJob = require('../services/cron-jobs/ads.service');

const app = express();
const httpServer = http.createServer(app);
getIo(httpServer);

if (config.env !== 'test') {
  app.use(morgan.successHandler);
  app.use(morgan.errorHandler);
}

// set security HTTP headers
app.use(helmet());

// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// sanitize request data
app.use(xss());
app.use(mongoSanitize());

// gzip compression
app.use(compression());

app.use(
  expressSession({
    secret: config.session_secret,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 7,
      secure: false,
      httpOnly: false,
    },
  })
);

// app.use(cookieParser);

// enable cors
app.use(
  cors({
    origin: '*',
  })
);
app.options('*', cors());

// jwt authentication
app.use(passport.initialize());

// initialize oauth services here
initPassport();
facebookAuth();
googleAuth();

passport.use('jwt', jwtStrategy);

// limit repeated failed requests to auth endpoints
if (config.env === 'production') {
  app.use('/v1/auth', authLimiter);
}

// v1 api routes
app.use('/api/v1', routes);

// send back a 404 error for any unknown api request
app.use((req, res) => {
  res.status(404).send(new ApiError(httpStatus.NOT_FOUND, 'Not found'));
});

// convert error to ApiError, if needed
app.use(errorConverter);

// handle error
app.use(errorHandler);

// connectQueue();

// config.mongoose.url
const startApp = () => {
  mongoose.connect(config.mongoose.url, config.mongoose.options).then(() => {
    logger.info('Connected to MongoDB');
    httpServer.listen(config.port, () => {
      logger.info(`Listening to port ${config.port}`);
    });
  });
};

// start cron jobs
const cron = () => {
  adCronJob();
};

const exitHandler = () => {
  if (httpServer) {
    httpServer.close(() => {
      logger.info('server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (httpServer) {
    httpServer.close();
  }
});

/**
 * UNCOMMENT WHEN THE APPLICATION LAUNCHES
 */
// cron();
startApp();
