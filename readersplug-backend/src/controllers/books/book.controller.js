const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { bookService, userService, notificationService, notificationInfo } = require('../../services');
const { uploadSingle, deleteSingle } = require('../../libs/cloudinary');
const { MESSAGES } = require('../../constants/responseMessages');

// TODO: set authorization as amiddleware

const createBook = catchAsync(async (req, res) => {
  if (req.user.role === 'reader') throw new ApiError(httpStatus.BAD_REQUEST, 'cannot publish books, user is a reader');
  const { file } = req;
  const data = { ...req.body, author: req.user._id };
  if (file) {
    const { url } = await uploadSingle(file.path);
    data.cover = url;
  }

  const book = await bookService.createBook(data);
  if (!book) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(httpStatus.CREATED).json({ message: MESSAGES.SUCCESS });
});

/**
 * @constant {object} filter gets the [author], [type] and [mature] query fields
 * these fields allow querying books by author, type and matureContent respectively
 */
const getBooks = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['author', 'type', 'mature']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await bookService.queryBooks(filter, options);
  res.status(200).json(result);
});

const getBookById = catchAsync(async (req, res) => {
  const book = await bookService.getBookById(req.params.bookId);
  if (!book) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(book);
});

const getBookByTitle = catchAsync(async (req, res) => {
  const book = await bookService.getBookByTitle(req.params.name);
  if (!book) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(book);
});

const queryBookByTitle = catchAsync(async (req, res) => {
  const books = await bookService.getBooksByTitle(req.params.name);
  if (!books.length) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(books);
});

const queryBookByCategories = catchAsync(async (req, res) => {
  const filter = {
    category: { $in: req.query.category },
  };
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const books = await bookService.getBooksByCategories(filter, options);
  if (!books) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(books);
});

const markBookCompleted = catchAsync(async (req, res) => {
  const book = await bookService.updateBookById(req.params.bookId, { completed: true });
  if (!book) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UPDATE_FAILED);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const updateBook = catchAsync(async (req, res) => {
  const book = await bookService.updateBookById(req.params.bookId, req.body);
  res.status(200).json(book);
});

const deleteBook = catchAsync(async (req, res) => {
  await bookService.deleteBookById(req.params.bookId);
  res.status(httpStatus.NO_CONTENT).json({ message: MESSAGES.DELETED });
});

const updateProfile = catchAsync(async (req, res, next) => {
  const book = await bookService.updateBookById(req.book.id, req.body);
  if (!book) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.UPDATE_FAILED));
  res.json({ message: MESSAGES.UPDATED });
});

const likedBook = catchAsync(async (req, res, next) => {
  const result = await bookService.like(req.params.bookId, req.user._id);
  if (result.modifiedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL));
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const dislikeBook = catchAsync(async (req, res, next) => {
  const result = await bookService.dislike(req.params.bookId, req.user._id);
  if (result.modifiedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL));
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const viewBook = catchAsync(async (req, res, next) => {
  const result = await bookService.incrementViews(req.params.bookId);
  if (result.matchedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL));
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const publishBook = catchAsync(async (req, res, next) => {
  const result = await bookService.publishBook(req.params.bookId);

  if (!result) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL));

  // json notification to user's followers
  const userFollowers = await userService.getUserFollowers(req.user._id);
  // would run a benchmark to test efficiency and speed of this code below
  if (userFollowers) {
    userFollowers.docs.map(async (user) => {
      await notificationService.createNotification({
        image: notificationInfo.notificationInfo.bookImage,
        message: `${req.user.username} ${notificationInfo.notificationInfo.publishedBook} "${result.title}"`,
        link: 'http',
        userId: user._id,
      });
    });
  }

  res.status(200).json({ message: MESSAGES.SUCCESS });
});

/**
 * Make share to work like likes and dislike
 */
const incrementShareNo = catchAsync(async (req, res, next) => {
  const result = await bookService.updateBook(req.params.id, { $inc: { nbShares: 1 } });
  if (!result) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL));
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const incrementRating = catchAsync(async (req, res, next) => {
  const result = await bookService.updateBook(req.params.id, { $inc: { rating: 1 } });
  if (!result) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL));
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

// ================================
// FROM HERE, SERVICES ARE FOR CHAPTERS
// ================================
/**
 * @body [title, content, nbPages, no] are required fields
 */
const addChapter = catchAsync(async (req, res, next) => {
  const { content, title, nbPages, no } = req.body;
  const { bookId } = req.params;

  let image = '';

  if (req.file) {
    // eslint-disable-next-line no-unused-vars
    const { publicId, url } = await uploadSingle(req.file.path);
    image = url;
  }

  const chapter = await bookService.addChapter({ content, bookId, title, nbPages, image, no });
  if (!chapter) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL));
  res.status(201).json({ message: MESSAGES.SUCCESS });
});

const getChapter = catchAsync(async (req, res, next) => {
  const { chapterId } = req.query;
  const { bookId } = req.params;
  const chapter = await bookService.getChapter(chapterId, bookId);
  if (!chapter) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING));
  res.status(200).json(chapter);
});

const queryBookChapters = catchAsync(async (req, res, next) => {
  const { limit, page, title, sortedBy, orderBy } = req.query;
  const chapters = await bookService.queryChapters({ limit, page }, { title, sortedBy, orderBy });
  if (!chapters) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING));
  res.status(200).json(chapters);
});

const queryBookChaptersByNo = catchAsync(async (req, res, next) => {
  const { bookId, no } = req.query;
  const chapter = await bookService.queryChaptersByPageNo(no, bookId);
  if (!chapter) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING));
  res.status(200).json(chapter);
});

const deleteChapter = catchAsync(async (req, res, next) => {
  const { chapterId } = req.params;

  // eslint-disable-next-line no-shadow
  const getChapter = await bookService.getChapterById(chapterId);
  if (!getChapter) return next(new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING));

  // posibility for a bug to exist here as the default image set in the model can cause a delete error if image wasn't uploaded by author
  await deleteSingle(getChapter.image);

  const result = await bookService.deleteChapter(chapterId, getChapter.bookId);
  if (result.deletedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.DELETE_FAILED));
  res.status(httpStatus.OK).json({ message: MESSAGES.DELETED });
});

const getChaptersForABook = catchAsync(async (req, res) => {
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const chapters = await bookService.getChaptersOfABook({ bookId: req.params.bookId }, options);
  if (!chapters) throw new ApiError(httpStatus.NOT_FOUND, 'Book has no chapters');
  res.status(httpStatus.OK).json(chapters);
});

const getLikesAndDislikes = catchAsync(async (req, res) => {
  const options = pick(req.query, ['limit', 'page']);
  options.populate = [
    { path: 'likes.userId', select: 'author username _id' },
    { path: 'dislikes.userId', select: 'author username _id' },
  ];
  options.select = ['likes', 'dislikes', 'totalLikes', 'totalDislikes'];
  const result = await bookService.getLikesAndDislikes({ _id: req.query.bookId }, options);
  if (!result) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(result);
});

module.exports = {
  createBook,
  getBooks,
  getBookById,
  updateBook,
  deleteBook,
  updateProfile,
  getBookByTitle,
  queryBookByTitle,
  queryBookByCategories,
  getLikesAndDislikes,
  markBookCompleted,
  likedBook,
  viewBook,
  dislikeBook,
  addChapter,
  getChapter,
  queryBookChapters,
  deleteChapter,
  getChaptersForABook,
  publishBook,
  incrementShareNo,
  incrementRating,
  queryBookChaptersByNo,
};
