const httpStatus = require('http-status');
const { User, Follow, DeletedAcc } = require('../models');
const ApiError = require('../utils/ApiError');
const myCustomLabels = require('../utils/labelPaginate');
const { MESSAGES } = require('../constants/responseMessages');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
// TODO: get birth month, day nd year and store separately
const createUser = async (userBody) => {
  if (await User.isEmailTaken(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, MESSAGES.EMAIL_TAKEN);
  }
  const user = await User.create({ ...userBody, password: 'Default123' });
  return user;
};

const isUsernameTaken = async (username) => {
  const user = await User.findOne({ username });
  if (user) throw new ApiError(httpStatus.BAD_REQUEST, 'username taken');

  return 'available';
};

const getUserByUsername = async (username) => {
  const user = await User.findOne({ username }).select([
    'avatar',
    'name',
    'username',
    'email',
    'type',
    'location',
    'totalFollowers',
    'totalFollowings',
  ]);
  return user;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryUsers = async ({ search, filter }, { limit, page, orderBy, sortedBy }) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };
  const users = await User.paginate(
    {
      $or: [
        { name: { $regex: search, $options: 'i' } },
        { email: { $regex: search, $options: 'i' } },
        { role: { $regex: search, $options: 'i' } },
      ],
      ...filter,
    },
    {
      ...(limit ? { limit } : { limit: 10 }),
      page,
      sort: { [orderBy]: sortedBy === 'asc' ? 1 : -1 },
      ...options,
    }
  );
  return users;
};

/**
 * Get user by id
 * @param {ObjectId} id
 * @returns {Promise<User>}
 */
const getUserById = async (id) => {
  return User.findById(id);
};

const returnUserData = async (id) => {
  return User.findById(id).select(['-password', '-name', '-defaultCurrency', '-walletBalance', '-updatedAt']);
};

const getUserByEmail = async (email) => {
  return User.findOne({ email });
};

/**
 * Update user by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateUserById = async (userId, updateBody) => {
  const user = await getUserById(userId);
  if (!user) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.USER_NOT_FOUND);
  if (updateBody.email && (await User.isEmailTaken(updateBody.email, userId))) {
    throw new ApiError(httpStatus.BAD_REQUEST, MESSAGES.EMAIL_TAKEN);
  }
  await isUsernameTaken(updateBody.username);

  Object.assign(user, updateBody);
  await user.save();
  return user;
};

const updateUserByEmail = async (email, updateBody) => {
  const user = await getUserByEmail(email);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.USER_NOT_FOUND);
  }
  await isUsernameTaken(updateBody.username);
  Object.assign(user, updateBody);
  await user.save();
  return user;
};

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const deleteUserById = async (userId) => {
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.USER_NOT_FOUND);
  }
  await user.remove();
  return user;
};

/**
 *
 * @param {string} currentUser represents the user making a reqquest
 * @param {string} userId is the id of the user that's being followed
 * @returns true or error
 */
const followUser = async (currentUser, userId) => {
  const session = await User.startSession();
  const transactionOptions = {
    readPreference: 'primary',
    readConcern: { level: 'local' },
    writeConcern: { w: 'majority' },
  };
  // eslint-disable-next-line one-var
  let user1, user2;
  try {
    await session.withTransaction(async () => {
      // don't update from findOneAndUpdate to updateOne because of the use of the user docs in controller
      user1 = await User.findOneAndUpdate({ _id: userId }, { $inc: { totalFollowers: 1 } }, session);
      if (!user1) {
        await session.abortTransaction();
        throw new Error('could not accept request');
      }

      // don't update from findOneAndUpdate to updateOne because of the use of the user docs in controller
      user2 = await User.findOneAndUpdate({ _id: currentUser }, { $inc: { totalFollowings: 1 } }, session);

      if (!user2) {
        await session.abortTransaction();
        throw new Error('could not accept request');
      }

      await Follow.create({
        followedUser: userId,
        followingUser: currentUser,
      });
    }, transactionOptions);
    return { username: user1.username, userId: user2._id };
  } catch (e) {
    throw new Error(e);
  } finally {
    session.endSession();
  }
};

/**
 * NOTE: i think there is a bug here......
 * @param {string} currentUser represents the user making a reqquest
 * @param {string} userId is the id of the user that wants to unfollow a user
 * @returns true or error
 */
const unfollow = async (currentUser, userId) => {
  const session = await User.startSession();
  const transactionOptions = {
    readPreference: 'primary',
    readConcern: { level: 'local' },
    writeConcern: { w: 'majority' },
  };
  try {
    await session.withTransaction(async () => {
      const user1 = await User.updateOne({ _id: userId }, { $inc: { totalFollowers: -1 } }, session);

      if (user1.modifiedCount === 0) {
        await session.abortTransaction();
        throw new Error('could not unfollow user');
      }

      const user2 = await User.updateOne({ _id: currentUser }, { $inc: { totalFollowings: -1 } }, session);

      if (user2.modifiedCount === 0) {
        await session.abortTransaction();
        throw new Error('could not unfollow user');
      }

      await Follow.deleteOne({ $and: [{ followedUser: userId }, { followingUser: currentUser }] });
    }, transactionOptions);
    return true;
  } catch (e) {
    throw new Error(e);
  } finally {
    session.endSession();
  }
};

const getUserFollowers = async (filter, options) => {
  const followers = await Follow.paginate({ ...filter }, { ...options });
  return followers;
};

const getUserFollowings = async (filter, options) => {
  const followings = await Follow.paginate({ ...filter }, { ...options });
  return followings;
};

const toggleMatureContents = async (user) => {
  const matureContents = user.showMatureContent;
  // eslint-disable-next-line no-param-reassign
  user.showMatureContent = !matureContents;
  return user.save();
};

const changeCurrency = async (user, currency) => {
  // eslint-disable-next-line no-param-reassign
  user.defaultCurrency = currency;
  return user.save();
};

const deleteAccount = async (data) => {
  const user = await User.findOne({ email: data.email });
  if (user.id !== data.userId) throw new ApiError(httpStatus.UNAUTHORIZED, 'user does not have permission');
  await user.remove();
  return DeletedAcc.create(data);
};

module.exports = {
  createUser,
  queryUsers,
  getUserById,
  returnUserData,
  updateUserByEmail,
  getUserByEmail,
  updateUserById,
  deleteUserById,
  followUser,
  unfollow,
  getUserFollowers,
  getUserFollowings,
  toggleMatureContents,
  changeCurrency,
  getUserByUsername,
  deleteAccount,
};
