const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { communityPostService, notificationInfo } = require('../../services');
const { uploadMany, uploadSingle } = require('../../libs/cloudinary');
const { sendData } = require('../../libs/messaging/rabbitMq-server');
const { createNotification } = require('../../services/notifications.service');
const { MESSAGES } = require('../../constants/responseMessages');

/**
 *  Notification Queue Name
 */
const notificationQueue = 'notification-queue';

const createPost = catchAsync(async (req, res) => {
  const { files, body } = req;
  const data = { content: body.content, author: req.user._id, communityId: body.communityId };

  if (files.length) {
    const filePaths = files.map((file) => file.path);
    const result = await uploadMany(filePaths);
    const multipleFiles = result.map((file) => ({ url: file.url, publicId: file.publicId }));
    Object.assign(data, { file: multipleFiles });
  }
  const post = await communityPostService.createPost(data);
  if (!post) throw new ApiError(httpStatus.EXPECTATION_FAILED, MESSAGES.UNSUCCESSFUL);
  res.status(201).json({ messgae: MESSAGES.SUCCESS });
});

const queryPosts = catchAsync(async (req, res) => {
  const { search, limit, page, filter, sortedBy, orderBy } = req.query;
  const { communityId } = req.params;
  const posts = await communityPostService.getPosts({ search, filter, communityId }, { limit, page, orderBy, sortedBy });
  if (!posts) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(posts);
});

const getPostById = catchAsync(async (req, res) => {
  const post = await communityPostService.getPostById(req.params.id);
  if (!post) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(post);
});

const updatePosts = catchAsync(async (req, res) => {
  const post = await communityPostService.updatePost({ content: req.body.content }, req.params.id);
  if (post.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UPDATE_FAILED);
  res.status(200).json({ mesage: MESSAGES.UPDATED });
});

const deletePost = catchAsync(async (req, res) => {
  const post = await communityPostService.deletePost(req.params.id);
  if (post.deletedCount !== 1) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.DELETE_FAILED);
  res.status(200).json({ message: MESSAGES.DELETED });
});

const likePost = catchAsync(async (req, res) => {
  const { user } = req;
  const isPost = await communityPostService.isPostLikedByUser(user._id, req.params.id);
  if (isPost) throw new ApiError(httpStatus.BAD_REQUEST, MESSAGES.UNSUCCESSFUL);

  const opts = {
    $addToSet: { likes: { userId: user._id } },
    $inc: { totalLikes: 1 },
  };

  const post = await communityPostService.updatePostAndReturn(opts, req.params.id);
  if (!post) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);

  const notificationData = {
    image: notificationInfo.notificationInfo.likeImage,
    message: `${req.user.username} ${notificationInfo.notificationInfo.likedPost}`,
    link: 'http', // would be updated later
    userId: post.author,
  };
  sendData(notificationQueue, notificationData);
  //   const data = await createNotification();
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const unlikePost = catchAsync(async (req, res) => {
  const { user } = req;
  const isPost = await communityPostService.isPostLikedByUser(user._id, req.params.id);
  if (!isPost) throw new ApiError(httpStatus.BAD_REQUEST, "user hasn't liked post");

  const post = await communityPostService.updatePost(
    {
      $pull: { likes: { userId: user._id } },
      $inc: { totalLikes: -1 },
    },
    req.params.id
  );

  if (post.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

// this controller gets both likes and shares and in the future would alos get comments
const getPostLikes = catchAsync(async (req, res) => {
  const likes = await communityPostService.getPostLikes(req.params.id);
  if (!likes) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(likes);
});

const sharePost = catchAsync(async (req, res) => {
  const { user, file, body } = req;
  const { communityId, postId } = req.params;
  let media = {};

  if (file) {
    const { url, publicId } = await uploadSingle(file.path);
    media = {
      file: {
        url,
        publicId,
      },
    };
  }

  const post = await communityPostService.shareAPost(postId, user._id, communityId, { ...body, ...media });
  if (post.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(201).json({ message: MESSAGES.CREATED });
});

module.exports = {
  createPost,
  queryPosts,
  getPostById,
  updatePosts,
  deletePost,
  likePost,
  unlikePost,
  getPostLikes,
  sharePost,
};
