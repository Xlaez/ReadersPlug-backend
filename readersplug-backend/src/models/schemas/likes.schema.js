const { Schema } = require('mongoose');
const modelNames = require('../../constants/modelNames.constants');

const likeSchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
    },
    likedAt: {
      type: Date,
      default: Date.now(),
    },
  },
  { _id: false }
);

module.exports = likeSchema;
