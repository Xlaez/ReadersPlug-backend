const mjml = require('mjml');
const { resolve } = require('path');
const { readFileSync } = require('fs');
const { compile } = require('handlebars');

/**
 * Require all templates and compile them
 */

const welcomeWaitlist = readFileSync(resolve(__dirname, '../templates/added-to-waitlist.mjml')).toString();
const verfiyAccountViaDigits = readFileSync(resolve(__dirname, '../templates//verify-ccount.mjml')).toString();

const welcomeWaitlistTemplate = compile(mjml(welcomeWaitlist).html);
const verifyViaDigitsTemplate = compile(mjml(verfiyAccountViaDigits).html);

module.exports = {
  welcomeWaitlistTemplate,
  verifyViaDigitsTemplate,
};
