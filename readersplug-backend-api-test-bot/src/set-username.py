import random
import time
import traceback
import requests
import csv

users=[]

with open("src/followers.csv") as data_file:
    data = csv.reader(data_file)
    for row in data:
        user ={
            # "name": row[0],
            "email": row[0],
            "username": row[1],
            "password": row[2],
        }
        users.append(user)

n = 0
params = {"words": 10, "paragraphs": 1, "format": "json"}

for user in users:
    n += 1
    if n :
        time.sleep(1)
        try:
            print("Adding {}".format(user["email"]))
            res = requests.patch(f"http://localhost:8080/api/v1/auth/set-password", data=user,verify=False)
            if res.status_code != 200 or res.status_code != 201:
                print(res.text)
            else:
                print(res.json())
            time.sleep(random.randrange(0, 1))
        except PermissionError:
            print('permission error................')
        except:
            traceback.print_exc()
            print("Unexpected error")
            continue
