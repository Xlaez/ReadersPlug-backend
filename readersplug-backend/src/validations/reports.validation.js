const Joi = require('joi');

const makeGroupReport = {
  body: Joi.object().keys({
    msgId: Joi.string().required(),
    reason: Joi.string().required(),
    groupId: Joi.string().required(),
  }),
};

module.exports = {
  makeGroupReport,
};
