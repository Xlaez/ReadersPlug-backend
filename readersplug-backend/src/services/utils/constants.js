const notificationInfo = {
  likedImage: 'your image has been liked by',
  requestImage: 'user', // would be updated
  userImage: 'user',
  communityImage: 'community',
  likeImage: 'thumb',
  bookImage: 'book',
  commentImage: 'chat',
  likedPost: 'liked your post',
  followed: 'has followed you',
  publishedBook: 'has published a new book titled',
  addAdmin: 'you have been made an admin in',
  communityRequest: 'sent a request to join',
  communityRequestAccepted: 'has accepted your request to join',
};

module.exports = {
  notificationInfo,
};
