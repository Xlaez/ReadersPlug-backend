const { GroupReport } = require('../../models');

const createReport = async (reporterId, reportedMsgId, groupId, reason) => {
  return GroupReport.create({
    reporterId,
    reportedMsgId,
    groupId,
    reason,
  });
};

const getAllReports = async (filter, options) => {
  return GroupReport.paginate(filter, options);
};

const getOneReport = async (reportId) => {
  return GroupReport.findById(reportId).populate('reportedMsgId');
};

const deleteReport = async (reportId) => {
  return GroupReport.deleteOne({ _id: reportId });
};

module.exports = {
  createReport,
  getAllReports,
  getOneReport,
  deleteReport,
};
