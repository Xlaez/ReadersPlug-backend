const httpStatus = require('http-status');
const { adminService } = require('../services');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { MESSAGES } = require('../constants/responseMessages');

const suspendUser = catchAsync(async (req, res) => {
  const { userId, duration, reason } = req.body;
  const result = await adminService.suspendUser(userId, duration, reason);
  if (!result) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'an error occurrd');
  res.status(httpStatus.OK).json({ message: 'user has been suspended' });
});

const getSuspendedAccounts = catchAsync(async (req, res) => {
  const { date, limit, page } = req.query;
  const filter = { duration: { $gte: date } };
  const options = { limit, page };
  const accounts = await adminService.getSuspendedAccounts(filter, options);
  if (!accounts) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(httpStatus.OK).json(accounts);
});

const unSuspendUser = catchAsync(async (req, res) => {
  const { userId } = req.body;
  const result = await adminService.unSuspendUser(userId);
  if (!result) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'an error occurrd');
  res.status(httpStatus.OK).json({ message: 'user has been unsuspended' });
});

module.exports = {
  suspendUser,
  unSuspendUser,
  getSuspendedAccounts,
};
