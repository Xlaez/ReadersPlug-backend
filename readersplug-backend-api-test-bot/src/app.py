import random
import time
import traceback
import requests
import csv

users =[]

with open("src/users.csv") as data_file:
    data = csv.reader(data_file)
    for row in data:
        user ={
            "name": row[0],
            "email": row[1],
            "role": row[2],
            "phoneNumber": row[3],
            "gender": row[4],
            "birthday": row[5]
        }
        users.append(user)

n = 0
params = {"words": 10, "paragraphs": 1, "format": "json"}


for user in users:
    n += 1
    if n :
        time.sleep(1)
        try:
            print("Adding {}".format(user["name"]))
            response = requests.post("http://localhost:8080/api/v1/auth/register", verify=False, data= user)
            if response.status_code == 201:
                print("user added successfully")
            else:
                print("an error occured")
            time.sleep(random.randrange(0, 1))
        except PermissionError:
            print('permission error................')
        except:
            traceback.print_exc()
            print("Unexpected error")
            continue