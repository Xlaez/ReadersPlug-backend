const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const schema = new Schema({
  followedUser: {
    type: Schema.Types.ObjectId,
    ref: modelNames.user,
    // required: true,
  },
  followingUser: {
    type: Schema.Types.ObjectId,
    ref: modelNames.user,
    // required: true,
  },
  followedAt: {
    type: Date,
    default: new Date(),
  },
});

schema.plugin(mongoosePaginate);
schema.plugin(toJSON);

const followersModel = model(modelNames.follower, schema);

module.exports = followersModel;
