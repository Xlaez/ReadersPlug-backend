/* eslint-disable default-param-last */
/* eslint-disable no-useless-catch */
const amq = require('amqplib');
const { amqp } = require('../../config/config');

let channel;
let connection;

async function connectQueue(queueName = 'notification-queue') {
  try {
    connection = await amq.connect(amqp.uri);
    channel = await connection.createChannel();
    await channel.assertQueue(queueName);
  } catch (e) {
    throw e;
  }
}

async function sendData(queueName = 'notification-queue', data) {
  await channel.sendToQueue(queueName, Buffer.from(JSON.stringify(data)));
  // close channel and connection
  await channel.close();
  await connection.close();
}

async function connectConsumerQueue(queueName = 'notification-queue') {
  let returnData;
  try {
    connection = await amq.connect(amqp.uri);
    channel = await connection.createChannel();
    await channel.assertQueue(queueName);
    channel.consume(queueName, (data) => {
      returnData = Buffer.from(data.content);
      channel.ack(data);
    });
    return returnData;
  } catch (e) {
    throw e;
  }
}

module.exports = { connectQueue, connectConsumerQueue, sendData };
