/* eslint-disable no-shadow */
const { verify } = require('jsonwebtoken');
const { Server } = require('socket.io');
const { jwt } = require('../config/config');

const { convService, userService } = require('../services');

const onlineUsers = new Map();

const registerUser = (userId, clientId) => {
  if (!onlineUsers.has(userId)) {
    onlineUsers.set(userId, clientId);
  }
};

const removeUser = (userId) => {
  return onlineUsers.delete(userId);
};

// eslint-disable-next-line no-unused-vars
const getUser = (userId) => {
  return onlineUsers.get(userId);
};

// remember to implement sockets for notifications

const getUserConversations = async (userId) => {
  const conversations = await convService.getConvsByUserId(userId);
  return conversations;
};

const decrypt = async (token) => {
  const result = await verify(token, jwt.secret);
  return result;
};

const authenticate = async (client, next) => {
  try {
    if (!client.handshake.query.token) return next(new Error('provide authentication token'));

    const decodeToken = await decrypt(client.handshake.query.token);

    const user = await userService.getUserById(decodeToken.id);

    if (!user) return next(new Error('user not found'));

    // eslint-disable-next-line no-param-reassign
    client.data.user = user;
    return next();
  } catch (e) {
    const errors = ['TokenExpiredError', 'NotBeforeError', 'JsonWebTokenError'];
    if (errors.includes(e.name)) {
      // eslint-disable-next-line no-shadow
      const e = new Error('Please authenticate');
      return next(e);
    }
    next(e);
  }
};

module.exports = {
  getIo: (server) => {
    const io = new Server(server, {
      cors: {
        origin: '*',
      },
      allowEIO3: true,
      transports: ['websocket', 'polling'],
    });

    // eslint-disable-next-line no-unused-vars
    io.on('connection', (server) => {
      // eslint-disable-next-line no-console
      console.log(server);
    });
    io.use((client, next) => authenticate(client, next)).on('connection', async (client) => {
      // eslint-disable-next-line no-console
      console.log('connection made', client.id, client.data.user._id);
      const { _id } = client.data.user;

      // add user to list of active users
      registerUser(_id, client.id);
      // eslint-disable-next-line no-console
      console.log(onlineUsers);

      // fetch user conversations and chatrooms
      const conversations = await getUserConversations(_id);
      // console.log(conversations);
      if (conversations.length) {
        conversations.forEach((conv) => {
          client.join(String(conv._id));
        });
      }
      io.emit('active-users', onlineUsers);
      client.on('join-chat', ({ conversationId }) => {
        client.join(conversationId);
        // eslint-disable-next-line no-console
        console.log(conversationId);
      });
      client.on('new-message', (convId, payload) => {
        // eslint-disable-next-line no-console
        console.info(convId, payload);
        // emit message to client
        io.in(convId).emit('new-message', {
          convId,
          sender: client.data.user._id,
          payload,
        });
      });
      client.on('typing', (convId) => {
        io.in(convId).emit('typing', { convId, user: client.data.user._id });
      });
      client.on('message-seen', (convId, payload) => {
        io.in(convId).emit('message-seen', { convId, payload });
      });
      client.on('disconnect', () => {
        removeUser(client.id);
        io.emit('active-users', onlineUsers);
      });
    });
  },
};
