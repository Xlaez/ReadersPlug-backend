import UserController from '../controllers/users.controller';
import { Router } from '@dolphjs/core';

class UserRoute {
  public path?: string = '/v1/users';
  public router = Router();
  protected controller: UserController = new UserController();

  constructor() {
    this.Routes();
  }

  private Routes() {
    this.router.post(`${this.path}/add-user`, this.controller.addUser);
    this.router.get(`${this.path}`, this.controller.getAllUsers);
    //this.router.delete(`${this.path}/delete-users`, this.controller.deleteUsers);
  }
}

export default UserRoute;
