const { ConvReport } = require('../../models');

const createReport = async (reporterId, reportedMsgId, convId, reason) => {
  return ConvReport.create({
    reporterId,
    reportedMsgId,
    convId,
    reason,
  });
};

const getAllReports = async (filter, options) => {
  return ConvReport.paginate(filter, options);
};

const getOneReport = async (reportId) => {
  return ConvReport.findById(reportId).populate('reportedMsgId');
};

const deleteReport = async (reportId) => {
  return ConvReport.deleteOne({ _id: reportId });
};

module.exports = {
  createReport,
  getAllReports,
  getOneReport,
  deleteReport,
};
