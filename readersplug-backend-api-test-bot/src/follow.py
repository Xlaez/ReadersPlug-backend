import random
import time
import traceback
import requests
import csv

users =[]

with open("src/followers.csv") as data_file:
    data = csv.reader(data_file)
    for row in data:
        user ={
            # "name": row[0],
            "email": row[0],
            "password": row[2],
        }
        users.append(user)

n = 0
params = {"words": 10, "paragraphs": 1, "format": "json"}

user_id = input("Input the user to follow's id \n") 

for user in users:
    n += 1
    if n :
        time.sleep(1)
        try:
            print("Adding {}".format(user["email"]))
            response = requests.post("http://localhost:8080/api/v1/auth/login", verify=False, data= user)
            if response.status_code == 200:
                # print("user logged in successfully")
                ans = response.json()
                res = requests.post(f"http://localhost:8080/api/v1/users/follow/{user_id}", verify=False, headers={"x-auth-token": ans["tokens"]["access"]["token"]})
                if res.status_code != 200 or res.status_code != 201:
                    print(res.text)
                else:
                    print(res.json())
            else:
                print("an error occured")
            time.sleep(random.randrange(0, 1))
        except PermissionError:
            print('permission error................')
        except:
            traceback.print_exc()
            print("Unexpected error")
            continue