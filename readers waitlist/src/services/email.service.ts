import { createTransport } from 'nodemailer';
import sgMail from '@sendgrid/mail';
import { SEND_GRID_KEY, EMAIL_FROM, SMTP_PASS, SMTP_USER, FACEBOOK_LINK, LINKEDIN_LINK, TWITTER_LINK } from '../configs';
import User from '../models/users.model';
import { AppRes } from '@dolphjs/core';
import mjml from 'mjml';
import { resolve } from 'path';
import { readFileSync } from 'fs';
import { compile } from 'handlebars';

const rawEmailTemplate = readFileSync(resolve(__dirname, '../../templates/template-design.mjml')).toString();

const processedEmailTemplate = compile(mjml(rawEmailTemplate).html);
const transport = createTransport({
  service: 'gmail',
  auth: {
    type: 'Login',
    user: SMTP_USER,
    pass: SMTP_PASS,
  },
});

const userRegistered = (to: string, subject: string) => {
  const facebook = FACEBOOK_LINK;
  const twitter = TWITTER_LINK;
  const linkedin = LINKEDIN_LINK;
  const html = processedEmailTemplate({ facebook, linkedin, twitter });
  const obj = { from: EMAIL_FROM, to, subject, html };
  return transport.sendMail(obj);
};

sgMail.setApiKey(SEND_GRID_KEY);

const sendUserAkWithSendGrid = (to: string, subject: string) => {
  const facebook = FACEBOOK_LINK;
  const twitter = TWITTER_LINK;
  const linkedin = LINKEDIN_LINK;
  const html = processedEmailTemplate({ facebook, linkedin, twitter });
  const msg = {
    to,
    subject,
    html,
    from: EMAIL_FROM,
  };
  return sgMail.send(msg);
};

const getUsers = async (currentTab) => {
  const perEmail = 100;

  const count = await User.find({ subscribed: true }).countDocuments();
  const users = await User.find({ subscribed: true })
    .skip((+currentTab - 1) * perEmail)
    .limit(perEmail)
    .lean();
  const remainingPages = Math.ceil(count / perEmail);
  return { count, users, remainingPages };
};

// TODO: parameters would be updated to support html
const sendNewsletter = async (subject, text) => {
  const { count, users, remainingPages } = await getUsers(1);
  const messages = [];
  if (count > 0) {
    users.forEach((user) => {
      messages.push({
        from: EMAIL_FROM,
        to: user.email,
        subject,
        text,
      });
    });
  }

  const sendMail = async () => {
    try {
      return sgMail.send(messages);
    } catch (error) {
      throw new AppRes(500, error);
    }
  };
  sendMail();
};

export default { sendNewsletter, userRegistered, sendUserAkWithSendGrid };
