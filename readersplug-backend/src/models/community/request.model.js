const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const schema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
    communityId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.community,
      required: true,
    },
  },
  {
    timestamps: true,
    id: false,
  }
);

schema.plugin(mongoosePaginate);
schema.plugin(toJSON);

const requestModel = model(modelNames.community_request, schema);

module.exports = {
  requestModel,
};
