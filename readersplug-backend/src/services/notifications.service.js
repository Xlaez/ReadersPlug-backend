/* eslint-disable no-unused-vars */
const httpStatus = require('http-status');
const { Notify } = require('../models');
const ApiError = require('../utils/ApiError');
const myCustomLabels = require('../utils/labelPaginate');
const { connectConsumerQueue } = require('../libs/messaging/rabbitMq-server');

/**
 * save notifications to database
 * @param {string} userId
 * @returns {Promise<Notice>}
 * */

const createNotification = async () => {
  const data = await connectConsumerQueue('notification-queue');
  console.log(data);
  //   if (!image && !link && !message && !userId)
  //     throw new ApiError(httpStatus.BAD_REQUEST, 'cannot send notification with incomplete fields');
  //   return Notify.create({
  //     image,
  //     link,
  //     message,
  //     userId,
  //   });
};

// find All waitlist
/**
 * Get notification by user
 * @param {string} userId
 * @returns {Promise<Notice>}
 */
const findAllNotification = async (userId, limit, page, orderBy, sortedBy) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };

  const notifications = await Notify.paginate(
    {
      userId,
      //   isSeen: false,
    },
    {
      ...(limit ? { limit } : { limit: 10 }),
      page,
      sort: { [orderBy]: sortedBy === 'asc' ? 1 : -1 },
      ...options,
    }
  );
  return notifications;
};

// find One Notification
/**
 * Get notification by user
 * @param {string} userId
 * @returns {Promise<Notice>}
 */
const findOneNotification = async (notificationId) => {
  const NotifyA = await Notify.findOne({ _id: notificationId });
  return NotifyA;
};

/**
 * @param {string} userId
 * @returns {Promise<Notice>}
 */
const deleteOne = async (notificationId) => {
  return Notify.deleteOne({ _id: notificationId });
};

const updateNotification = async (id) => {
  return Notify.updateOne({ _id: id }, { isSeen: true });
};

const setNotificationAsSeen = async (userId) => {
  const result = await Notify.updateMany({ userId }, { isSeen: true });
  return result;
};

module.exports = {
  createNotification,
  findAllNotification,
  findOneNotification,
  deleteOne,
  updateNotification,
  setNotificationAsSeen,
};
