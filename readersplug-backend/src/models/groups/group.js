const { Schema, model } = require('mongoose');
const aggregatePagination = require('mongoose-aggregate-paginate-v2');
const pagination = require('mongoose-paginate-v2');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const schema = new Schema({
  members: [
    {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
  ],
  admins: [
    {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
  ],
  muteIds: [
    {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
    },
  ],
  logo: {
    url: {
      type: String,
    },
    publicId: {
      type: String,
    },
  },
  name: {
    type: String,
    required: true,
  },
  blockIds: [
    {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
    },
  ],
});

schema.plugin(pagination);
schema.plugin(aggregatePagination);
schema.plugin(toJSON);

schema.methods.addAdmin = async function (userId) {
  this.admins.push = userId;
  return this.save();
};

const GroupRoom = model(modelNames.chat_group, schema);
module.exports = GroupRoom;
