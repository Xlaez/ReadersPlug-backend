const httpStatus = require('http-status');
const catchAsync = require('../../utils/catchAsync');
const { conversationReportService } = require('../../services');
const ApiError = require('../../utils/ApiError');
const pick = require('../../utils/pick');

const makeReport = catchAsync(async (req, res, next) => {
  const { body, user } = req;

  const result = await conversationReportService.createReport(user._id, body.msgId, body.convId, body.reason);

  if (!result) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot make a report'));
  res.status(httpStatus.OK).send('reported');
});

const getReports = catchAsync(async (req, res, next) => {
  const options = pick(req.query, ['limit', 'page']);
  const result = await conversationReportService.getAllReports({}, options);
  if (!result) return next(new ApiError(httpStatus.NOT_FOUND, 'resources not found'));
  res.status(httpStatus.OK).send(result);
});

const getReport = catchAsync(async (req, res, next) => {
  const report = await conversationReportService.getOneReport(req.params.id);
  if (!report) return next(new ApiError(httpStatus.NOT_FOUND, 'resource not found'));
  res.status(httpStatus.OK).send(report);
});

const deleteReport = catchAsync(async (req, res, next) => {
  const result = await conversationReportService.deleteReport(req.params.id);
  if (result.deletedCount === 0) return next(new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot delete report'));
  res.status(httpStatus.OK).send('deleted');
});

module.exports = {
  makeReport,
  getReport,
  getReports,
  deleteReport,
};
