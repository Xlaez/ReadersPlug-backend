const Joi = require('joi');

const createAd = {
  body: Joi.object().keys({
    title: Joi.string().required(),
    content: Joi.string().required(),
    location: Joi.string().required(),
    expiresAt: Joi.date().required(),
    type: Joi.string(),
  }),
};

const getAds = {
  query: Joi.object().keys({
    page: Joi.string().required(),
    limit: Joi.string().required(),
    location: Joi.string().required(),
  }),
};

module.exports = {
  createAd,
  getAds,
};
