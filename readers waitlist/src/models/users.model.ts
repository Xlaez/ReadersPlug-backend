import { Schema, model, Document } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

export interface IUser extends Document {
  email: string;
  subscribed: boolean; // used for newsletter
}

const schema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    subscribed: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

schema.plugin(mongoosePaginate);

const Users = model<IUser>('users', schema);
export default Users;
