const kue = require('kue');
const cluster = require('cluster');
const http = require('http');
const numCpus = require('os').cpus();

const queue = kue.createQueue();

function performTask(taskData, done) {
  // Task here
  done();
}

queue.process('email', (job, done) => {
  performTask(job.data, done);
});

if (cluster.isMaster) {
  console.log(`Mastr ${process.pid}`);

  for (let i = 0; i < numCpus; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  http.createServer((req, res) => {}).listen();
  console.log(`Worker ${process.pid}`);
}
