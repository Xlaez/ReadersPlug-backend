const express = require('express');
const validate = require('../../middlewares/validate');
const { reportValidation } = require('../../validations');
const { groupReportController } = require('../../controllers');

// eslint-disable-next-line no-unused-vars
const auth = require('../../middlewares/auth');
const validateAcc = require('../../middlewares/validateUser');

const router = express.Router();

router.post('/', validateAcc, validate(reportValidation.makeGroupReport), groupReportController.makeReport);
router.get('/', validateAcc, groupReportController.getReports);
router.get('/:id', validateAcc, groupReportController.getReport);
router.delete('/:id', validateAcc, groupReportController.deleteReport);

module.exports = router;
