const express = require('express');
// eslint-disable-next-line no-unused-vars
const auth = require('../../middlewares/auth');
const validateAcc = require('../../middlewares/validateUser');
const { noteController } = require('../../controllers');

const router = express.Router();

router.post('/', validateAcc, noteController.saveNote);
router.get('/by-userId', validateAcc, noteController.findAllByUserId);
router.get('/:noteId', validateAcc, noteController.findOneByNotesId);
router.delete('/:noteId', validateAcc, noteController.deleteOneByNotesId);

module.exports = router;
