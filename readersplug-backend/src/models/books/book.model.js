const mongoose = require('mongoose');
const validator = require('validator');
const mongoosePaginate = require('mongoose-paginate-v2');
const categories = require('./catgories');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const likeSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: modelNames.user,
    },
    at: {
      type: Date,
      default: Date.now(),
    },
  },
  { _id: false }
);

const bookSchema = mongoose.Schema(
  {
    author: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: modelNames.user,
      required: true,
    },
    title: {
      type: String,
      required: true,
      trim: true,
    },
    cover: {
      type: String,
      default:
        'https://images.unsplash.com/photo-1589998059171-988d887df646?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Nnx8fGVufDB8fHx8&auto=format&fit=crop&w=500&q=60',
    },
    description: {
      type: String,
    },
    category: {
      type: [String],
      enum: categories,
      required: true,
    },
    published: {
      type: Boolean,
      default: false,
    },
    publishedAt: {
      type: Date,
      default: null,
    },
    views: {
      type: Number,
      default: 0,
    },
    likes: [likeSchema],
    dislikes: [likeSchema],
    totalLikes: {
      type: Number,
      default: 0,
    },
    totalDislikes: {
      type: Number,
      default: 0,
    },
    nbComments: {
      type: Number,
      default: 0,
    },
    promoted: {
      type: Boolean,
      default: false,
    },
    promote: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Promote',
    },
    nbChapters: {
      type: Number,
      default: 0,
    },
    type: {
      type: String,
      enum: ['free', 'premium'],
      default: 'free',
    },
    nbShares: {
      type: Number,
      default: 0,
    },
    rating: {
      type: Number,
      default: 0,
    },
    language: {
      type: String,
      default: 'english',
    },
    tags: {
      type: [String],
    },
    completed: {
      type: Boolean,
      default: false,
    },
    isbn: {
      type: String,
      maxlength: 13,
      minlength: 10,
      Validate(value) {
        if (!validator.isISBN(value)) {
          throw new Error('ISBN is not valid');
        }
      },
    },
    year: {
      type: Number,
      default: new Date().getFullYear(),
    },
    mature: {
      type: Boolean,
      default: false,
    },
    status: {
      type: String,
      enum: ['active', 'inactive'],
      default: 'active',
    },
    price: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
bookSchema.plugin(mongoosePaginate);
bookSchema.plugin(toJSON);

bookSchema.statics.hasUserLiked = async function (id, userId) {
  const result = await this.findOne({ $and: [{ _id: id }, { likes: { $in: { userId } } }] });
  return !!result;
};

bookSchema.statics.hasUserDisLiked = async function (id, userId) {
  const result = await this.findOne({ $and: [{ _id: id }, { dislikes: { $in: { userId } } }] });
  return !!result;
};

/**
 * Check if name & author is taken
 * @param {string} name & author - The user's name & author
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
bookSchema.statics.isNameTaken = async function (name, author, excludeUserId) {
  const book = await this.findOne({ name, author, _id: { $ne: excludeUserId } });
  return !!book;
};

/**
 * @typedef Book
 */
const Book = mongoose.model(modelNames.book, bookSchema);

module.exports = Book;
