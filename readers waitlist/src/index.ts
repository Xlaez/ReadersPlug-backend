import { ENV, PORT, MONGO_URI } from './configs';
import mongoConfig from './configs/db';
import helmet from 'helmet';
import Dolph from '@dolphjs/core';
import NewsletterRoute from './routes/newsletter.routes';
import UserRoute from './routes/users.routes';
const mongoose = require('mongoose');

const server = new Dolph([new UserRoute(), new NewsletterRoute()], ENV, 'development', null, null);
console.log("test");
server.initExternalMiddleWares([]);
mongoose
  .connect(mongoConfig.url, mongoConfig.options)
  .then(() => {
    server.listen();
    console.log('connected');
  })
  .catch();
