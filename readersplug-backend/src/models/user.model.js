const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const mongoosePaginate = require('mongoose-paginate-v2');
const categories = require('./books/catgories');
const modelNames = require('../constants/modelNames.constants');
// const { roles } = require('../config/roles');
const toJSON = require('./plugins/toJSON.plugin');

const userSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Invalid email');
        }
      },
    },
    password: {
      type: String,
      required: true,
      trim: true,
      //   minlength: 8,  commented beacsue of passport oauth
      validate(value) {
        if (value !== 'none') {
          if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
            throw new Error('Password must contain at least one letter and one number');
          }
        }
      },
      private: true, // used by the toJSON plugin
    },
    role: {
      type: String,
      enum: ['reader', 'author', 'both', 'admin'], // an admin can perform all other role privilledges
      default: 'both',
    },
    permissions: {
      type: [String],
      default: [],
    },
    isEmailVerified: {
      type: Boolean,
      default: false,
    },
    isSuspended: {
      type: Boolean,
      default: false,
    },
    avatar: {
      url: {
        type: String,
      },
      publicId: {
        type: String,
      },
    },
    bio: {
      type: String,
      minlenght: 100,
      maxlength: 250,
    },
    dob: {
      type: Date,
    },
    location: {
      type: String,
    },
    interests: [
      {
        type: String,
        enum: categories,
      },
    ],
    gender: {
      type: String,
      minlenght: 1,
      enum: ['male', 'female', 'not-say'],
    },
    phoneNumber: {
      type: String,
      trim: true,
    },
    username: {
      type: String,
      minlenght: 2,
      // unique: true,
    },
    totalFollowers: {
      type: Number,
      default: 0,
    },
    totalFollowings: {
      type: Number,
      default: 0,
    },
    defaultCurrency: {
      type: String,
      default: 'USD',
    },
    showMatureContent: {
      type: Boolean,
      default: false,
    },
    accountType: {
      type: String,
      enum: ['basic', 'premium'],
      default: 'basic',
    },
    provider: {
      type: String,
      default: 'login',
      enum: ['login', 'google', 'apple', 'facebook'],
    },
    providerToken: {
      type: String,
      required: false,
    },
    walletBalance: {
      type: Number,
      default: 0,
    },
    accountDetails: {
      palmPayMail: {
        type: String,
      },
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
userSchema.plugin(mongoosePaginate);
userSchema.plugin(toJSON);

/**
 * Check if email is taken
 * @param {string} email - The user's email
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
userSchema.statics.isEmailTaken = async function (email, excludeUserId) {
  const user = await this.findOne({ email, _id: { $ne: excludeUserId } });
  return !!user;
};

/**
 * Check if password matches the user's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};

userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 11);
  }
  next();
});

/**
 * @typedef User
 */
const User = mongoose.model(modelNames.user, userSchema);

module.exports = User;
