const httpStatus = require('http-status');
const { adService } = require('../services');
const catchAsync = require('../utils/catchAsync');
const { uploadSingle } = require('../libs/cloudinary');
const ApiError = require('../utils/ApiError');
const pick = require('../utils/pick');

// NOTE: In the future payment would be integrated
const createAd = catchAsync(async (req, res) => {
  const { file, body, user } = req;
  const uploadObj = { ...body };
  uploadObj.content = {};
  uploadObj.content.text = body.content;
  uploadObj.ownerId = user._id;
  uploadObj.locations = [body.location];
  if (file) {
    const { url } = await uploadSingle(file.path);
    uploadObj.content.file = url;
    await adService.createAd(uploadObj);
  } else {
    await adService.createAd(uploadObj);
  }
  res.status(httpStatus.CREATED).send();
});

const getAdById = catchAsync(async (req, res) => {
  const data = await adService.getAdById(req.params.id);
  if (!data) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');
  res.status(httpStatus.OK).send(data);
});

const getAds = catchAsync(async (req, res) => {
  const filter = { locations: { $in: req.query.location } };
  const options = pick(req.query, ['limit', 'page']);
  options.populate = { path: 'ownerId', select: 'username avatar _id' };
  const ads = await adService.getAds(filter, options);
  if (!ads) throw new ApiError(httpStatus.NOT_FOUND, 'resource not found');
  res.status(httpStatus.OK).send(ads);
});

const deleteAd = catchAsync(async (req, res) => {
  const result = await adService.deleteAd(req.user._id, req.params.id);
  if (result.deletedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, 'cannot delete ad');
  res.status(httpStatus.OK).send();
});

module.exports = {
  createAd,
  getAdById,
  getAds,
  deleteAd,
};
