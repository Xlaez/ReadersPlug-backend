const { Schema, model } = require('mongoose');
const aggregatePagination = require('mongoose-aggregate-paginate-v2');
const pagination = require('mongoose-paginate-v2');
const modelNames = require('../constants/modelNames.constants');
const toJSON = require('./plugins/toJSON.plugin');

const schema = new Schema(
  {
    members: [
      {
        type: Schema.Types.ObjectId,
        ref: modelNames.user,
        required: true,
      },
    ],
    blocked: [
      {
        type: Boolean,
        default: false,
      },
    ],
    blockedIds: [
      {
        type: Schema.Types.ObjectId,
        ref: modelNames.user,
      },
    ],
  },
  {
    timestamps: true,
  }
);

schema.plugin(pagination);
schema.plugin(aggregatePagination);
schema.plugin(toJSON);

const convModel = model(modelNames.conversation, schema);
module.exports = convModel;
