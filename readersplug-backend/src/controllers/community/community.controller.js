const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { communityService, notificationService, notificationInfo } = require('../../services');
const { uploadSingle, deleteSingle } = require('../../libs/cloudinary');
const { MESSAGES } = require('../../constants/responseMessages');

const createCommunity = catchAsync(async (req, res) => {
  const { body } = req;
  const isName = await communityService.getCommunityByName(body.name);
  if (isName) throw new ApiError(httpStatus.BAD_REQUEST, 'name already taken');

  await communityService.saveCommunity({
    ...body,
    admins: { id: [req.user._id] },
    adminCount: 1,
  });
  if (!body) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(201).json({ message: MESSAGES.SUCCESS });
});

const queryCommunities = catchAsync(async (req, res) => {
  const { search, limit, page, filter, sortedBy, orderBy } = req.query;
  const communities = await communityService.queryCommunities({ search, filter }, { limit, page, orderBy, sortedBy });
  if (!communities) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(communities);
});

const uploadImage = catchAsync(async (req, res) => {
  const { file } = req;
  const { publicId, url } = await uploadSingle(file.path);
  const community = await communityService.uploadImage(req.params.id, url, publicId);
  if (community.modifiedCount !== 1) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const updateInfo = catchAsync(async (req, res) => {
  const { id } = req.params;
  const community = await communityService.updateCommunity(id, { info: req.body.info });
  if (!community) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UPDATE_FAILED);
  res.status(200).json({ message: MESSAGES.UPDATED });
});

const updateRulesAndType = catchAsync(async (req, res) => {
  const { id } = req.params;

  let community;

  if (req.body.rules && req.body.type) {
    community = await communityService.updateCommunity(id, { rules: req.body.rules, type: req.body.type });
  } else if (req.body.rules) {
    community = await communityService.updateCommunity(id, { rules: req.body.rules });
  } else if (req.body.type) {
    community = await communityService.updateCommunity(id, { type: req.body.type });
  }

  if (!community) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UPDATE_FAILED);
  res.status(200).json({ message: MESSAGES.UPDATED });
});

const getCommunityByName = catchAsync(async (req, res) => {
  const { name } = req.params;
  const community = await communityService.getCommunityByName(name);
  if (!community) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(community);
});

const getCommunityById = catchAsync(async (req, res) => {
  const { id } = req.params;
  const community = await communityService.getACommunityById(id);
  if (!community) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(community);
});

const addMember = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.updateCommunity(id, {
    $addToSet: { members: { id: req.body.member } },
    $inc: { membersCount: 1 },
  });

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).send(MESSAGES.SUCCESS);
});

const addAdmin = catchAsync(async (req, res) => {
  const { id } = req.params;

  const admins = [];

  // eslint-disable-next-line array-callback-return, no-shadow
  req.body.admin.map((id) => {
    admins.push({ id });
  });

  const community = await communityService.updateCommunity(id, {
    $push: { admins },
    $inc: { adminCount: admins.length },
  });

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);

  // check for a better way to do this
  req.body.admin.map(async (user) => {
    await notificationService.createNotification({
      image: notificationInfo.notificationInfo.communityImage,
      message: ` ${notificationInfo.notificationInfo.addAdmin} ${community.name} by ${req.user.username}`,
      link: 'http', // would be updated later
      userId: user,
    });
  });
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const removeMember = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.updateCommunity(id, {
    $pull: { members: { id: req.body.member } },
    $inc: { membersCount: -1 },
  });

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const removeAdmin = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.updateCommunity(id, {
    $pull: { admins: { id: req.body.admin } },
    $inc: { adminCount: -1 },
  });

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const getMembers = catchAsync(async (req, res) => {
  const { id } = req.params;
  const community = await communityService.getMembers(id);
  if (!community) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(community);
});

const sendRequestToCommunity = catchAsync(async (req, res) => {
  const { id } = req.params;

  const request = await communityService.sendRequestTo(req.user._id, id);

  if (!request) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.UNSUCCESSFUL);

  // check for improvements in implementation
  request.admins.map(async (user) => {
    await notificationService.createNotification({
      image: notificationInfo.notificationInfo.communityImage,
      message: `${req.user.username} ${notificationInfo.notificationInfo.communityRequest} ${request.name}`,
      link: 'http', // would be updated later
      userId: user.id,
    });
  });

  res.status(201).json({ message: MESSAGES.SUCCESS });
});

/**
 * TODO: I think there is a bug here that stores userId as null
 */
const acceptRequest = catchAsync(async (req, res) => {
  const { communityId, requestId, userId } = req.params;

  const community = await communityService.updateCommunity(communityId, {
    $addToSet: { members: { id: userId } },
    $inc: { membersCount: 1 },
  });

  await communityService.deleteRequest(requestId);

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);

  await notificationService.createNotification({
    image: notificationInfo.notificationInfo.communityImage,
    message: `${req.user.username} ${notificationInfo.notificationInfo.communityRequestAccepted} ${community.name}`,
    link: 'http', // would be updated later
    userId,
  });

  res.status(200).json({ message: MESSAGES.SUCCESS });
});

const getRequests = catchAsync(async (req, res) => {
  const { id } = req.params;
  const { limit, page } = req.query;
  const requests = await communityService.getAllRequests({ id, limit, page });
  if (!requests) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);
  res.status(200).json(requests);
});

const rejectRequest = catchAsync(async (req, res) => {
  const { id } = req.params;
  const request = await communityService.deleteRequest(id);
  if (request.modifiedCount === 0) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, MESSAGES.DELETE_FAILED);
  res.status(200).json({ message: MESSAGES.DELETED });
});

const deleteCommunity = catchAsync(async (req, res) => {
  const { id } = req.params;

  const community = await communityService.getACommunityById(id);

  if (!community) throw new ApiError(httpStatus.NOT_FOUND, MESSAGES.RESOURCE_MISSING);

  const result = await deleteSingle(community.coverImage.url);

  if (!result) throw new ApiError(httpStatus.EXPECTATION_FAILED, MESSAGES.DELETE_FAILED);

  await communityService.deleteCommunity(community._id);

  res.status(200).json({ message: MESSAGES.DELETED });
});

module.exports = {
  createCommunity,
  queryCommunities,
  uploadImage,
  getCommunityByName,
  getCommunityById,
  addMember,
  removeMember,
  getMembers,
  deleteCommunity,
  addAdmin,
  removeAdmin,
  updateInfo,
  updateRulesAndType,
  sendRequestToCommunity,
  acceptRequest,
  getRequests,
  rejectRequest,
};
