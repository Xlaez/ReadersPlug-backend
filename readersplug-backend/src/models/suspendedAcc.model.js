const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const modelNames = require('../constants/modelNames.constants');
const toJSON = require('./plugins/toJSON.plugin');

const schema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.user,
      required: true,
    },
    reason: {
      type: String,
      required: true,
    },
    duration: {
      type: Date,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

schema.plugin(mongoosePaginate);
schema.plugin(toJSON);

const Admin = model(modelNames.suspendedAcc, schema);
module.exports = Admin;
