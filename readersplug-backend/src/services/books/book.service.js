const httpStatus = require('http-status');
const myCustomLabels = require('../../utils/labelPaginate');

const { Book, Chapter } = require('../../models');
const ApiError = require('../../utils/ApiError');

/**
 * Create a book
 * @param {Object} bookBody
 * @returns {Promise<Book>}
 */
const createBook = async (bookBody) => {
  if (await Book.isNameTaken(bookBody.title + bookBody.author)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Book already exists');
  }
  return Book.create(bookBody);
};

/**
 * Query for books
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryBooks = async (filter, options) => {
  const books = await Book.paginate(filter, options);
  return books;
};

/**
 * Get book by id
 * @param {ObjectId} id
 * @returns {Promise<Book>}
 */
const getBookById = async (id) => {
  return Book.findById(id);
};

/**
 * Get book by author
 * @param {string} author
 * @returns {Promise<Book>}
 */
const getBookByAutor = async (author) => {
  return Book.findOne({ author });
};

/**
 * Get book by title
 * @param {string} title
 * @returns {Promise<Book>}
 */
const getBookByTitle = async (title) => {
  return Book.findOne({ title });
};

// TODO: paginate
const getBooksByTitle = async (title) => {
  return Book.find({ title: { $regex: title, $options: 'i' } });
};

/**
 * Query for books
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */

const getBooksByCategories = async (filter, options) => {
  const books = Book.paginate(filter, options);
  return books;
};

/**
 * Update book by id
 * @param {ObjectId} bookId
 * @param {Object} updateBody
 * @returns {Promise<Book>}
 */
const updateBookById = async (bookId, updateBody) => {
  const book = await getBookById(bookId);
  if (!book) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Book not found');
  }
  if (updateBody.title && (await Book.isNameTaken(updateBody.title + book.author, book.id))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Book already exists');
  }
  Object.assign(book, updateBody);
  await book.save();
  return book;
};
/**
 *
 * @param {string} bookId
 * @param {object} updateObj
 * @returns {Promise<UpdateResult>}
 */
const updateBook = async (bookId, updateObj) => {
  const result = await Book.updateOne({ _id: bookId }, updateObj);
  return result;
};

/**
 * Delete book by id
 * @param {ObjectId} bookId
 * @returns {Promise<Book>}
 */
const deleteBookById = async (bookId) => {
  const book = await getBookById(bookId);
  if (!book) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Book not found');
  }
  await book.remove();
  return book;
};

const hasLiked = async (bookId, userId) => {
  const result = await Book.findOne({ $and: [{ _id: bookId }, { 'likes.userId': { $in: userId } }] });
  return result;
};

const hasDisliked = async (bookId, userId) => {
  const result = await Book.findOne({ $and: [{ _id: bookId }, { 'dislikes.userId': { $in: userId } }] });
  return result;
};

/**
 * Note: potential bug for the like functionality
 */
const like = async (bookId, userId) => {
  if (await Book.hasUserDisLiked(bookId, userId)) {
    return Book.updateOne(
      { _id: bookId },
      {
        $inc: { totalDislikes: -1, totalLikes: 1 },
        $pull: {
          dislikes: { userId },
        },
        $addToSet: { likes: { userId } },
      }
    );
  }
  if (await Book.hasUserLiked(bookId, userId)) {
    return Book.updateOne({ _id: bookId }, { $inc: { totalLikes: -1 }, $pull: { likes: { userId } } });
  }
  return Book.updateOne({ _id: bookId }, { $inc: { totalLikes: 1 }, $addToSet: { likes: { userId } } });
};

const dislike = async (bookId, userId) => {
  if (await Book.hasUserLiked(bookId, userId)) {
    return Book.updateOne(
      { _id: bookId },
      {
        $inc: { totalDislikes: 1, totalLikes: -1 },
        $addToSet: {
          dislikes: { userId },
        },
        $pull: { likes: { userId } },
      }
    );
  }
  if (await Book.hasUserDisLiked(bookId, userId)) {
    return Book.updateOne({ _id: bookId }, { $inc: { totalDislikes: -1 }, $pull: { dislikes: { userId } } });
  }
  return Book.updateOne({ _id: bookId }, { $inc: { totalDislikes: 1 }, $addToSet: { dislikes: { userId } } });
};

const incrementViews = async (bookId) => {
  const result = await Book.updateOne({ _id: bookId }, { $inc: { views: 1 } });
  return result;
};

const addChapter = async ({ content, bookId, title, image, nbPages, no }) => {
  const bookChapter = await Chapter.create({
    bookId,
    content,
    image,
    nbPages,
    title,
    chapterNum: no,
  });
  await Book.updateOne({ _id: bookId }, { $inc: { nbChapters: 1 } });
  return bookChapter;
};

const deleteChapter = async (chapterId, bookId) => {
  const chapter = await Chapter.deleteOne({ _id: chapterId });
  await Book.updateOne({ _id: bookId }, { $inc: { nbChapters: -1 } });

  return chapter;
};

const getChapter = async (chapterId, bookId) => {
  const chapter = await Chapter.findOne({ $and: [{ _id: chapterId }, { bookId }] }).populate(
    'bookId',
    'author title cover description language nbChapters'
  );
  return chapter;
};

const getChapterById = async (chapterId) => {
  const chapter = await Chapter.findById(chapterId);
  return chapter;
};

const queryChapters = async ({ limit, page }, { title, sortedBy, orderBy }) => {
  const options = {
    lean: true,
    customLabels: myCustomLabels,
  };
  let filter = {};

  if (title) {
    filter = { title: { $regex: title, $options: 'i' } };
  }
  const chapters = await Chapter.paginate(filter, {
    ...(limit ? { limit } : { limit: 20 }),
    page,
    sort: { [orderBy]: sortedBy === 'asc' ? 1 : -1 },
    ...options,
  });
  return chapters;
};

const getChaptersOfABook = async (filter, options) => {
  const chapters = await Chapter.paginate(filter, options);
  return chapters;
};

/**
 *TODO: update name to suit purpose
 */
const queryChaptersByPageNo = async (chapterNum, bookId) => {
  const chapter = await Chapter.find({ $and: [{ chapterNum }, { bookId }] })
    .populate('bookId', 'title id author')
    .lean();
  return chapter;
};

const publishBook = async (bookId) => {
  const book = await Book.findOneAndUpdate({ _id: bookId }, { publishedAt: Date.now(), published: true });
  return book;
};

const getLikesAndDislikes = async (filter, options) => {
  return Book.paginate(filter, options);
};

module.exports = {
  createBook,
  queryBooks,
  getBookById,
  getBookByAutor,
  getBookByTitle,
  updateBookById,
  deleteBookById,
  getBooksByTitle,
  getBooksByCategories,
  getLikesAndDislikes,
  like,
  dislike,
  hasDisliked,
  hasLiked,
  incrementViews,
  // From here, all services belong to the book chapter implmentation
  addChapter,
  deleteChapter,
  getChapter,
  queryChapters,
  getChaptersOfABook,
  getChapterById,
  publishBook,
  updateBook,
  queryChaptersByPageNo,
};
