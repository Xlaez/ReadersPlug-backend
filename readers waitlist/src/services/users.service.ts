import Users from '../models/users.model';

class UserService {
  private model = Users;

  public isEmailTaken = async (email: string) => {
    return this.model.findOne({ email });
  };

  public addUser = async (email: string) => {
    return this.model.create({ email });
  };

  // TODO: remember to add ts generic for paginate in model and remove ignore statement
  public getUsers = async (filter: any, options: any) => {
    // @ts-expect-error
    const users = await this.model.paginate(filter, options);
    return users;
  };

  // public deleteAllUsers = async () => {
  //   return this.model.deleteMany({});
}

export default UserService;
