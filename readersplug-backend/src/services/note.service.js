const { Note } = require('../models');

const createNote = async (data) => {
  const note = await Note.create(data);
  return note;
};

const findOneByNotesId = async (noteId) => {
  // update this code so that it uses findById
  const note = await Note.findOne({ _id: noteId });
  return note;
};

const findAllByUserId = async (filter, options) => {
  return Note.paginate(filter, options);
};

const deleteOneByNotesId = async (noteId) => {
  const deleteNote = await Note.deleteOne({ _id: noteId });
  return deleteNote;
};

module.exports = {
  createNote,
  findOneByNotesId,
  findAllByUserId,
  deleteOneByNotesId,
};
