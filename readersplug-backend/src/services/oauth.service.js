/* eslint-disable camelcase */
/* eslint-disable no-console */
const passport = require('passport');
const passportFacebook = require('passport-facebook');
const passportGoogle = require('passport-google-oauth20');
const jwksClient = require('jwks-rsa');
const { decode, verify } = require('jsonwebtoken');
const httpStatus = require('http-status');
const config = require('../config/config');
const { createUser, getUserByEmail } = require('./user.service');
const { tokenService } = require('.');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

exports.createCookie = (token) => {
  return `x-auth-token=${token};path=/;HttpOnly;Max-Age=${config.cookie_duration || 60 * 60 * 24 * 7}`;
};

exports.initPassport = () => {
  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });
};
// TODO: login user if already exist in database
exports.facebookAuth = () => {
  try {
    const strategy = new passportFacebook.Strategy(
      {
        ...config.facebook,
        profileFields: ['id', 'email', 'name', 'picture.type(large)'],
        scope: ['email', 'public_profile'],
        enableProof: true,
      },
      async (accessToken, _refreshToken, profile, done) => {
        const user = await getUserByEmail(profile.emails[0].value);

        if (user) {
          const tokens = await tokenService.generateAuthTokens(user);
          const cookie = this.createCookie(tokens.access.token);
          return done(null, { user, tokens, cookie });
        }
        const userBody = {
          'avatar.url': profile.photos[0].value,
          provider: 'facebook',
          providerToken: accessToken,
          password: 'none',
          name: profile.name.givenName,
          role: 'both',
          email: profile.emails[0].value,
          gender: profile.user_gender,
          location: profile.user_location,
          isEmailVerified: true,
        };

        const userProfile = await createUser(userBody);

        if (!userProfile) return done('an error occured: cannot save user to database', null);

        const tokens = await tokenService.generateAuthTokens(userProfile);
        const cookie = this.createCookie(tokens.access.token);
        // console.log(tokens, cookie, userProfile);
        return done(null, { user: userProfile, tokens, cookie });
      }
    );
    passport.use(strategy);
  } catch (error) {
    ApiError(httpStatus.INTERNAL_SERVER_ERROR, error);
  }
};

exports.googleAuth = () => {
  const googleStrategy = new passportGoogle.Strategy(
    {
      ...config.google,
      scope: ['email', 'profile'],
      //   passReqToCallback: true,
    },
    async (_request, accessToken, _refreshToken, profile, done) => {
      const user = await getUserByEmail(profile.emails[0].value);
      if (user) {
        const tokens = await tokenService.generateAuthTokens(user);
        const cookie = this.createCookie(tokens.access.token);
        return done(null, { user, tokens, cookie });
      }
      const userBody = {
        'avatar.url': profile.photos[0].value,
        provider: 'google',
        providerToken: accessToken,
        password: 'none',
        name: profile.displayName,
        role: 'both',
        email: profile.emails[0].value,
        gender: profile.user_gender,
        location: profile.user_location,
        isEmailVerified: true,
      };

      const userProfile = await createUser(userBody);
      if (!userProfile) return done('an error occured: cannot save user to database', null);

      const tokens = await tokenService.generateAuthTokens(userProfile);
      const cookie = this.createCookie(tokens.access.token);
      // console.log(tokens, cookie, userProfile);
      return done(null, { user: userProfile, tokens, cookie });
    }
  );
  passport.use(googleStrategy);
};

const appleKey = async (kid) => {
  const client = jwksClient({
    jwksUri: 'https://appleid.apple.com/auth/keys',
    timeout: 30000,
  });
  return client.getSigningKey(kid);
};

exports.appleAuth = catchAsync(async (req, res) => {
  const { id_token } = req.body;
  const { header } = decode(id_token, {
    complete: true,
  });
  const { kid } = header;
  const publicKey = (await appleKey(kid)).getPublicKey();

  const { sub, email } = verify(id_token, publicKey);

  const userBody = {
    provider: 'apple',
    providerToken: sub,
    password: 'none',
    role: 'both',
    email,
    isEmailVerified: true,
  };

  const userProfile = await createUser(userBody);
  if (!userProfile) throw new ApiError(500, 'an error occured: cannot save user to database');

  const tokens = await tokenService.generateAuthTokens(userProfile);
  res.status(200).json({ tokens });
});
