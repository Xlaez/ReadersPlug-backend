const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const modelNames = require('../../constants/modelNames.constants');
const toJSON = require('../plugins/toJSON.plugin');

const schema = new Schema(
  {
    bookId: {
      type: Schema.Types.ObjectId,
      ref: modelNames.book,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    content: {
      type: String,
      required: true,
    },
    nbPages: {
      type: Number,
      default: 0,
    },
    image: {
      type: String,
      default: 'https://google.com/unsplash',
    },
    chapterNum: {
      type: Number,
      default: 1,
    },
  },
  {
    timestamps: true,
  }
);

schema.plugin(mongoosePaginate);
schema.plugin(toJSON);

const chapterModel = model(modelNames.book_chapter, schema);

module.exports = chapterModel;
